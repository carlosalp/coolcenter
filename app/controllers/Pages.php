<?php 
    class Pages extends Controller{
        //Llamamos a los modelos y se les asigna una variable
        public function __construct(){
            $this->marcaModelo = $this->model('Marca');
            $this->grupoModelo = $this->model('Grupo');
            $this->subgrupoModelo = $this->model('Subgrupo');
            $this->articuloModelo = $this->model('Articulo');
            $this->proveedorModelo = $this->model('Proveedor');
            $this->tipoUnidadModelo = $this->model('TipoUnidad');
            $this->almacenModelo = $this->model('Almacen');
            $this->loginModelo = $this->model('Login');
            $this->tipoPagoModelo = $this->model('TipoPago');
            $this->estadoModelo = $this->model('Estado');
            $this->municipioModelo = $this->model('Municipio');
            $this->logoutModelo = $this->model('Logout');
            $this->compraModelo = $this->model('Compra');
            $this->ventaModelo = $this->model('Venta');
            $this->inventarioModelo = $this->model('Inventario');
            $this->clienteModelo = $this->model('Cliente');
            $this->empleadoModelo = $this->model('Empleado');
            $this->usuarioModelo = $this->model('Usuario');

            //$this->usuarioModelo = $this->model('Usuario');
        }
        
        

        function index(){
            
            $usuarios = $this->marcaModelo->getMarca();

            $datos = [
                'usuarios' => $usuarios
            ];

            $this->vista('pages/start', $datos);
        }
        

        


        //////////////////////////////////////////////////////FUNCION PARA IMAGENES AL INSERTAR
        
        function imagenInsert($nombre, $archivoName, $archivoNameTmp, $archivoTipo, $contador, $ruta){

            if (!file_exists($archivoNameTmp) || !is_uploaded_file($archivoNameTmp)){
                $imagen = "predeterminado.png";;
            }
            else{
                $ext = explode(".", $archivoName);
                if ($archivoTipo == "image/jpg" || $archivoTipo == "image/jpeg" || $archivoTipo == "image/png"){
                    $imagen = "C-" . $contador . " " . $nombre . '.' . end($ext);
                    move_uploaded_file($archivoNameTmp, $ruta . $imagen);
                }
            }
            return $imagen;
        }


        //////////////////////////////////////////////////////FUNCION PARA IMAGENES AL ACTUALIZAR
        
        function imagenUpdate($nombre, $archivoName, $archivoNameTmp, $archivoTipo, $contador, $ruta, $nombreimagen){
            if (!file_exists($archivoNameTmp) || !is_uploaded_file($archivoNameTmp)){
                if($nombreimagen != '')
                    $imagen = $nombreimagen;
                else
                    $imagen = "predeterminado.png";;
            }
            else{
                $ext = explode(".", $archivoName);
                if ($archivoTipo == "image/jpg" || $archivoTipo == "image/jpeg" || $archivoTipo == "image/png"){
                    $imagen = "C-" . $contador . " " . $nombre . '.' . end($ext);
                    move_uploaded_file($archivoNameTmp, $ruta . $imagen);
                }
            }
            return $imagen;
        }

        /////////////////////////////////////////////////////////////////////MARCA
        
        //Solicitamos las paginas a usar
        //require_once '../pages/marca.php';
        public function marca(){
            //Solicitamos la funcion para obtener datos de la tabla
            $marca = $this->marcaModelo->getMarca();
            //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
            $datos = [
                'marca' => $marca
            ];
    
            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['desc'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'descr' => trim($_POST['desc'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->marcaModelo->updateMarca($data)){
                        $this->vista('pages/marca', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->marcaModelo->deleteMarca($data)){
                        $this->vista('pages/marca', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'descr' => trim($_POST['desc']),
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->marcaModelo->setMarca($data)){
                        $this->vista('pages/marca', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'descr' => "",
                ];
    
                $this->vista('pages/marca', $datos);
            }
    
            $this->vista('pages/marca', $datos);
        }


        //////////////////////////////////////////////////////////////////////GRUPO

        public function grupo(){
            //Solicitamos la funcion para obtener datos de la tabla
            $grupo = $this->grupoModelo->getGrupo();
            //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
            $datos = [
                'grupo' => $grupo
            ];
    
            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['desc'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'descr' => trim($_POST['desc'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->grupoModelo->updateGrupo($data)){
                        $this->vista('pages/grupo', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->grupoModelo->deleteGrupo($data)){
                        $this->vista('pages/grupo', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'descr' => trim($_POST['desc']),
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->grupoModelo->setGrupo($data)){
                        $this->vista('pages/grupo', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'descr' => "",
                ];
    
                $this->vista('pages/grupo', $datos);
            }
    
            $this->vista('pages/grupo', $datos);
        }




        ////////////////////////////////////////////////////////////////////SUBGRUPO

        public function subgrupo(){
            //Solicitamos la funcion para obtener datos de la tabla
            $subgrupo = $this->subgrupoModelo->getSubgrupo();
            $datosgrupo = $this->grupoModelo->getGrupo();
            //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
            $datos = [
                'subgrupo' => $subgrupo,
                'grupo' => $datosgrupo
            ];
    
            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['desc'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'descr' => trim($_POST['desc']),
                        'grupo' => trim($_POST['grupo'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->subgrupoModelo->updateSubgrupo($data)){
                        $this->vista('pages/subgrupo', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->subgrupoModelo->deleteSubgrupo($data)){
                        $this->vista('pages/subgrupo', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'descr' => trim($_POST['desc']),
                        'grupo' => trim($_POST['grupo'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->subgrupoModelo->setSubgrupo($data)){
                        $this->vista('pages/subgrupo', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'descr' => "",
                    'grupo' => ""
                ];
    
                $this->vista('pages/subgrupo', $datos);
            }
    
            $this->vista('pages/subgrupo', $datos);
        }


        ////////////////////////////////////////////////////////////////////ARTICULO

        public function articulo(){
            //Solicitamos la funcion para obtener datos de la tabla
            $articulo = $this->articuloModelo->getArticulo();
            $subgrupo = $this->subgrupoModelo->getSubgrupo();
            $grupo = $this->grupoModelo->getGrupo();
            $marca = $this->marcaModelo->getMarca();
            $unidad = $this->tipoUnidadModelo->getTipoUnidad();
            //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
            $datos = [
                'articulo' => $articulo,
                'subgrupo' => $subgrupo,
                'grupo' => $grupo,
                'marca' => $marca,
                'tipounidad' => $unidad
            ];
    
            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $ruta = "imagenes/articulos/";
                $contador = $this->articuloModelo->getArticuloNums();
                $contador++;
                if(isset($_FILES['imagen']['tmp_name'])){
                    $archivoName = $_FILES['imagen']['name'];
                    $archivoNameTmp = $_FILES['imagen']['tmp_name'];
                    $archivoTipo = $_FILES['imagen']['type'];
                }
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['nombre'])){
                    //Obtenemos el numero de registros de la tabla
                    $nombre = trim($_POST['nombre']) . " " . trim($_POST['ns']);
                    //$archivo = $_FILES['imagen'];
                    $nombreimagen = trim($_POST['img']);
                    $imagen = $this->imagenUpdate($nombre, $archivoName, $archivoNameTmp, $archivoTipo, $contador, $ruta, $nombreimagen);
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'nombre' => trim($_POST['nombre']),
                        'ns' => trim($_POST['ns']),
                        'descr' => trim($_POST['desc']),
                        'modelo' => trim($_POST['modelo']),
                        'precio_venta' => trim($_POST['precio_venta']),
                        'subgrupo' => trim($_POST['subgrupo']),
                        'grupo' => trim($_POST['grupo']),
                        'marca' => trim($_POST['marca']),
                        //'precio_compra' => trim($_POST['precio_compra']),
                        'descuento' => trim($_POST['descuento']),
                        'unidad' => trim($_POST['unidad']),
                        'img' => trim($imagen)
                    ];                    
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->articuloModelo->updateArticulo($data)){
                        $this->vista('pages/articulo', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->articuloModelo->deleteArticulo($data)){
                        $this->vista('pages/articulo', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Obtenemos el numero de registros de la tabla
                    $nombre = trim($_POST['nombre']) . " " . trim($_POST['ns']);
                    //Movemos al archivo a una ruta del servidor y lo renombramos
                    $imagen = $this->imagenInsert($nombre, $archivoName, $archivoNameTmp, $archivoTipo, $contador, $ruta);
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'nombre' => trim($_POST['nombre']),
                        'ns' => trim($_POST['ns']),
                        'descr' => trim($_POST['desc']),
                        'modelo' => trim($_POST['modelo']),
                        'precio_venta' => trim($_POST['precio_venta']),
                        'subgrupo' => trim($_POST['subgrupo']),
                        'grupo' => trim($_POST['grupo']),
                        'marca' => trim($_POST['marca']),
                        //'precio_compra' => trim($_POST['precio_compra']),
                        'descuento' => trim($_POST['descuento']),
                        'unidad' => trim($_POST['unidad']),
                        'img' => $imagen
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->articuloModelo->setArticulo($data)){
                        $this->vista('pages/articulo', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'nombre' => "",
                    'ns' => "",
                    'descr' => "",
                    'modelo' => "",
                    'precio_venta' => "",
                    'subgrupo' => "",
                    'grupo' => "",
                    'marca' => "",
                    //'precio_compra' => "",
                    'descuento' => "",
                    'unidad' => "",
                    'img' => "",
                ];
    
                $this->vista('pages/articulo', $datos);
            }
    
            $this->vista('pages/articulo', $datos);
        }


        ////////////////////////////////////////////////////////////////////PROVEEDOR

        public function proveedor(){
            //Solicitamos la funcion para obtener datos de la tabla
            $proveedor = $this->proveedorModelo->getProveedor();
            $municipio = $this->municipioModelo->getMunicipio();
            $estado = $this->estadoModelo->getEstado();
            //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
            $datos = [
                'proveedor' => $proveedor,
                'municipio' => $municipio,
                'estado' => $estado
            ];
    
             //Validamos que se realizó una solicitud del metodo Post
             if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['desc'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'descr' => trim($_POST['desc']),
                        'municipio' => trim($_POST['municipio']),
                        'cp' => trim($_POST['cp']),
                        'estado' => trim($_POST['estado']),
                        'telefono' => trim($_POST['telefono']),
                        'rfc' => trim($_POST['rfc']),
                        'email' => trim($_POST['email']),
                        'credito' => trim($_POST['credito']),
                        'descuento' => trim($_POST['descuento']),
                        'direccion' => trim($_POST['direccion']),
                        'celular' => trim($_POST['celular'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->proveedorModelo->updateProveedor($data)){
                        $this->vista('pages/proveedor', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->proveedorModelo->deleteProveedor($data)){
                        $this->vista('pages/proveedor', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    
                    $data = [
                        'descr' => trim($_POST['desc']),
                        'municipio' => trim($_POST['municipio']),
                        'cp' => trim($_POST['cp']),
                        'estado' => trim($_POST['estado']),
                        'telefono' => trim($_POST['telefono']),
                        'rfc' => trim($_POST['rfc']),
                        'email' => trim($_POST['email']),
                        'credito' => trim($_POST['credito']),
                        'descuento' => trim($_POST['descuento']),
                        'direccion' => trim($_POST['direccion']),
                        'celular' => trim($_POST['celular'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->proveedorModelo->setProveedor($data)){
                        $this->vista('pages/proveedor', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'descr' => "",
                    'municipio' => "",
                    'cp' => "",
                    'estado' => "",
                    'telefono' => "",
                    'rfc' =>"",
                    'email' => "",
                    'credito' => "",
                    'descuento' => "",
                    'direccion' =>"",
                    'celular' => ""
                ];
    
                $this->vista('pages/proveedor', $datos);
            }
    
            $this->vista('pages/proveedor', $datos);
        }

        /////////////////////////////////////////////////////////////////////TIPO UNIDAD
        
        //Solicitamos las paginas a usar
        //require_once '../pages/marca.php';
        public function tipounidad(){
            //Solicitamos la funcion para obtener datos de la tabla
            $tipounidad = $this->tipoUnidadModelo->getTipoUnidad();
            //Creamos una matriz para colocar los datos obtenidos de la tabla TipoUnidad
            $datos = [
                'tipounidad' => $tipounidad
            ];
    
            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['nombre'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'nombre' => trim($_POST['nombre'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->tipoUnidadModelo->updateTipoUnidad($data)){
                        $this->vista('pages/tipounidad', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->tipoUnidadModelo->deleteTipoUnidad($data)){
                        $this->vista('pages/tipounidad', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'nombre' => trim($_POST['nombre']),
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->tipoUnidadModelo->setTipoUnidad($data)){
                        $this->vista('pages/tipounidad', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'nombre' => "",
                ];
    
                $this->vista('pages/tipounidad', $datos);
            }
    
            $this->vista('pages/tipounidad', $datos);
        }

         /////////////////////////////////////////////////////////////////////ALMACEN
        
        //Solicitamos las paginas a usar
        //require_once '../pages/marca.php';
        public function almacen(){
            //Solicitamos la funcion para obtener datos de la tabla
            $almacen = $this->almacenModelo->getAlmacen();
            //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
            $datos = [
                'almacen' => $almacen
            ];
    
            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['desc'])  && isset($_POST['direccion'])  && isset($_POST['telefono'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'descr' => trim($_POST['desc']),
                        'direccion' => trim($_POST['direccion']),
                        'telefono' => trim($_POST['telefono'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->almacenModelo->updateAlmacen($data)){
                        $this->vista('pages/almacen', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->almacenModelo->deleteAlmacen($data)){
                        $this->vista('pages/almacen', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'descr' => trim($_POST['desc']),
                        'direccion' => trim($_POST['direccion']),
                        'telefono' => trim($_POST['telefono'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->almacenModelo->setAlmacen($data)){
                        $this->vista('pages/almacen', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'descr' => "",
                    'direccion' => "",
                    'telefono' => "",
                ];
    
                $this->vista('pages/almacen', $datos);
            }
    
            $this->vista('pages/almacen', $datos);
        }

        //////////////////////////////////////////////////////////////////////TIPO DE PAGO

        public function tipopago(){
            //Solicitamos la funcion para obtener datos de la tabla
            $tipopago = $this->tipoPagoModelo->getTipoPago();
            //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
            $datos = [
                'tipopago' => $tipopago
            ];

            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['desc'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'descr' => trim($_POST['desc'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->tipoPagoModelo->updateTipoPago($data)){
                        $this->vista('pages/tipopago', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->tipoPagoModelo->deleteTipoPago($data)){
                        $this->vista('pages/tipopago', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'descr' => trim($_POST['desc']),
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->tipoPagoModelo->setTipoPago($data)){
                        $this->vista('pages/tipopago', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'descr' => "",
                ];

                $this->vista('pages/tipopago', $datos);
            }

            $this->vista('pages/tipopago', $datos);
        }

        //////////////////////////////////////////////////////////////////////ESTADO

        public function estado(){
            //Solicitamos la funcion para obtener datos de la tabla
            $estado = $this->estadoModelo->getEstado();
            //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
            $datos = [
                'estado' => $estado
            ];

            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['desc'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'descr' => trim($_POST['desc'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->estadoModelo->updateEstado($data)){
                        $this->vista('pages/estado', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->estadoModelo->deleteEstado($data)){
                        $this->vista('pages/estado', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'descr' => trim($_POST['desc']),
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->estadoModelo->setEstado($data)){
                        $this->vista('pages/estado', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'descr' => "",
                ];

                $this->vista('pages/estado', $datos);
            }

            $this->vista('pages/estado', $datos);
        }

    ////////////////////////////////////////////////////////////////////MUNICIPIO

    public function municipio(){
        //Solicitamos la funcion para obtener datos de la tabla
        $municipio = $this->municipioModelo->getMunicipio();
        $estado = $this->estadoModelo->getEstado();
        //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
        $datos = [
            'municipio' => $municipio,
            'estado' => $estado
        ];

        //Validamos que se realizó una solicitud del metodo Post
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //Para realizar una actualización validamos tener los datos a actualizar
            if(isset($_POST['id']) && isset($_POST['desc'])){
                //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                $data = [
                    'id' => trim($_POST['id']),
                    'descr' => trim($_POST['desc']),
                    'estado' => trim($_POST['estado'])
                ];
                //Validamos si la consulta se ejecuta correctamente 
                if($this->municipioModelo->updateMunicipio($data)){
                    $this->vista('pages/municipio', $datos);
                }else{
                    die('Algo salio mal');
                }
                //Para realizar una eliminación validamos el id
            }else if(isset($_POST['id'])){
                //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                $data = [
                    'id' => trim($_POST['id'])
                ];
                //Validamos si la consulta se ejecuta correctamente 
                if($this->municipioModelo->deleteMunicipio($data)){
                    $this->vista('pages/municipio', $datos);
                }else{
                    die('Algo salio mal');
                }
            }else{
                //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                $data = [
                    'descr' => trim($_POST['desc']),
                    'estado' => trim($_POST['estado'])
                ];
                //Validamos si la consulta se ejecuta correctamente 
                if($this->municipioModelo->setMunicipio($data)){
                    $this->vista('pages/municipio', $datos);
                }else{
                    die('Algo salio mal');
                }
            }
        }else{
            $data = [
                'descr' => "",
                'estado' => ""
            ];

            $this->vista('pages/municipio', $datos);
        }

        $this->vista('pages/municipio', $datos);
    }

    ////////////////////////////////////////////////////////////////////INVENTARIO

    public function inventario(){
        //Solicitamos la funcion para obtener datos de la tabla
        $inventario = $this->inventarioModelo->getInventario();
        $articulo = $this->articuloModelo->getArticulo();
        $almacen = $this->almacenModelo->getAlmacen();
        //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
        $datos = [
            'inventario' => $inventario,
            'articulo' => $articulo,
            'almacen' => $almacen
        ];

        //Validamos que se realizó una solicitud del metodo Post
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //Para realizar una actualización validamos tener los datos a actualizar
            if(isset($_POST['id']) && isset($_POST['articulo']) && isset($_POST['almacen'])){
                //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                $data = [
                    'id' => trim($_POST['id']),
                    'articulo' => trim($_POST['articulo']),
                    'cantidad' => trim($_POST['cantidad']),
                    'almacen' => trim($_POST['almacen'])
                ];
                //Validamos si la consulta se ejecuta correctamente 
                if($this->inventarioModelo->updateInventario($data)){
                    $this->vista('pages/inventario', $datos);
                }else{
                    die('Algo salio mal');
                }
                //Para realizar una eliminación validamos el id
            }else if(isset($_POST['id'])){
                //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                $data = [
                    'id' => trim($_POST['id'])
                ];
                //Validamos si la consulta se ejecuta correctamente 
                if($this->inventarioModelo->deleteInventario($data)){
                    $this->vista('pages/inventario', $datos);
                }else{
                    die('Algo salio mal');
                }
            }else{
                //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                $data = [
                    'articulo' => trim($_POST['articulo']),
                    'cantidad' => trim($_POST['cantidad']),
                    'almacen' => trim($_POST['almacen'])
                ];
                //Validamos si la consulta se ejecuta correctamente 
                if($this->inventarioModelo->setInventario($data)){
                    $this->vista('pages/inventario', $datos);
                }else{
                    die('Algo salio mal');
                }
            }
        }else{
            $data = [
                'articulo' => "",
                'cantidad' => "",
                'almacen' => ""
            ];
            $this->vista('pages/inventario', $datos);
        }
        $this->vista('pages/inventario', $datos);
    }

//
    public function exInventario(){
        $data = [
            'id' => trim($_POST['id'])
        ];
        $this->vista('pages/exInventario',$data);
    }

        ////////////////////////////////////////////////////////////////////CLIENTES

        public function cliente(){
            //Solicitamos la funcion para obtener datos de la tabla
            $cliente = $this->clienteModelo->getCliente();
            $municipio = $this->municipioModelo->getMunicipio();
            $estado = $this->estadoModelo->getEstado();
            //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
            $datos = [
                'cliente' => $cliente,
                'municipio' => $municipio,
                'estado' => $estado
            ];
    
             //Validamos que se realizó una solicitud del metodo Post
             if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['id']) && isset($_POST['desc'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id']),
                        'descr' => trim($_POST['desc']),
                        'municipio' => trim($_POST['municipio']),
                        'cp' => trim($_POST['cp']),
                        'estado' => trim($_POST['estado']),
                        'telefono' => trim($_POST['telefono']),
                        'rfc' => trim($_POST['rfc']),
                        'email' => trim($_POST['email']),
                        'credito' => trim($_POST['credito']),
                        'descuento' => trim($_POST['descuento']),
                        'direccion' => trim($_POST['direccion']),
                        'celular' => trim($_POST['celular'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->clienteModelo->updateCliente($data)){
                        $this->vista('pages/cliente', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                    //Para realizar una eliminación validamos el id
                }else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->clienteModelo->deleteCliente($data)){
                        $this->vista('pages/cliente', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    
                    $data = [
                        'descr' => trim($_POST['desc']),
                        'municipio' => trim($_POST['municipio']),
                        'cp' => trim($_POST['cp']),
                        'estado' => trim($_POST['estado']),
                        'telefono' => trim($_POST['telefono']),
                        'rfc' => trim($_POST['rfc']),
                        'email' => trim($_POST['email']),
                        'credito' => trim($_POST['credito']),
                        'descuento' => trim($_POST['descuento']),
                        'direccion' => trim($_POST['direccion']),
                        'celular' => trim($_POST['celular'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->clienteModelo->setCliente($data)){
                        $this->vista('pages/cliente', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
            }else{
                $data = [
                    'descr' => "",
                    'municipio' => "",
                    'cp' => "",
                    'estado' => "",
                    'telefono' => "",
                    'rfc' =>"",
                    'email' => "",
                    'credito' => "",
                    'descuento' => "",
                    'direccion' =>"",
                    'celular' => ""
                ];
    
                $this->vista('pages/cliente', $datos);
            }
    
            $this->vista('pages/cliente', $datos);
        }

    ////////////////////////////////////////////////////////////////////EMPLEADOS

                public function empleado(){
                    //Solicitamos la funcion para obtener datos de la tabla
                    $empleado = $this->empleadoModelo->getEmpleado();
                    $municipio = $this->municipioModelo->getMunicipio();
                    $estado = $this->estadoModelo->getEstado();
                    //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
                    $datos = [
                        'empleado' => $empleado,
                        'municipio' => $municipio,
                        'estado' => $estado
                    ];
            
                     //Validamos que se realizó una solicitud del metodo Post
                     if($_SERVER['REQUEST_METHOD'] == 'POST'){
                        //Para realizar una actualización validamos tener los datos a actualizar
                        if(isset($_POST['id']) && isset($_POST['desc'])){
                            //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                            $data = [
                                'id' => trim($_POST['id']),
                                'descr' => trim($_POST['desc']),
                                'municipio' => trim($_POST['municipio']),
                                'cp' => trim($_POST['cp']),
                                'estado' => trim($_POST['estado']),
                                'telefono' => trim($_POST['telefono']),
                                'rfc' => trim($_POST['rfc']),
                                'email' => trim($_POST['email']),
                                'credito' => trim($_POST['credito']),
                                'descuento' => trim($_POST['descuento']),
                                'direccion' => trim($_POST['direccion']),
                                'celular' => trim($_POST['celular'])
                            ];
                            //Validamos si la consulta se ejecuta correctamente 
                            if($this->empleadoModelo->updateEmpleado($data)){
                                $this->vista('pages/empleado', $datos);
                            }else{
                                die('Algo salio mal');
                            }
                            //Para realizar una eliminación validamos el id
                        }else if(isset($_POST['id'])){
                            //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                            $data = [
                                'id' => trim($_POST['id']),
                                'email' => trim($_POST['email'])
                            ];
                            //Validamos si la consulta se ejecuta correctamente 
                            if($this->empleadoModelo->deleteEmpleado($data)){
                                $this->vista('pages/empleado', $datos);
                            }else{
                                die('Algo salio mal');
                            }
                        }else{
                            //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                            
                            $data = [
                                'descr' => trim($_POST['desc']),
                                'municipio' => trim($_POST['municipio']),
                                'cp' => trim($_POST['cp']),
                                'estado' => trim($_POST['estado']),
                                'telefono' => trim($_POST['telefono']),
                                'rfc' => trim($_POST['rfc']),
                                'credito' => trim($_POST['credito']),
                                'descuento' => trim($_POST['descuento']),
                                'direccion' => trim($_POST['direccion']),
                                'celular' => trim($_POST['celular']),
                                'usuario' => trim($_POST['usuario']),
                                'email' => trim($_POST['email']),
                                'pass' => trim($_POST['pass']),
                                'nivel' => trim($_POST['nivel'])
                            ];
                            //Validamos si la consulta se ejecuta correctamente 
                            if($this->empleadoModelo->setEmpleado($data)){
                                $this->vista('pages/empleado', $datos);
                            }else{
                                die('Algo salio mal');
                            }
                        }
                    }else{
                        $data = [
                            'descr' => "",
                            'municipio' => "",
                            'cp' => "",
                            'estado' => "",
                            'telefono' => "",
                            'rfc' =>"",
                            'email' => "",
                            'credito' => "",
                            'descuento' => "",
                            'direccion' =>"",
                            'celular' => ""
                        ];
            
                        $this->vista('pages/empleado', $datos);
                    }
            
                    $this->vista('pages/empleado', $datos);
                }
        

   ////////////////////////////////////////////////////////////////////USUARIO

   public function usuario(){
    //Solicitamos la funcion para obtener datos de la tabla
    $usuario = $this->usuarioModelo->getUsuario();
    $empleado = $this->empleadoModelo->getEmpleado();
    //Creamos una matriz para colocar los datos obtenidos de la tabla Marca
    $datos = [
        'usuario' => $usuario,
        'empleado' => $empleado
    ];

    //Validamos que se realizó una solicitud del metodo Post
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        //Para realizar una actualización validamos tener los datos a actualizar
        if(isset($_POST['id']) && isset($_POST['desc'])){
            //Colocamos los datos obtenidos en una matriz para proceder al Modelo
            $data = [
                'nombre' => trim($_POST['nombre']),
                'usuario' => trim($_POST['usuario']),
                'email' => trim($_POST['email']),
                'pass' => trim($_POST['pass'])
            ];
            //Validamos si la consulta se ejecuta correctamente 
            if($this->usuarioModelo->updateUsuario($data)){
                $this->vista('pages/usuario', $datos);
            }else{
                die('Algo salio mal');
            }
        }
    }else{
        $data = [
            'nombre' => "",
            'usuario' => "",
            'email' => "",
            'pass' => ""
        ];
        $this->vista('pages/usuario', $datos);
    }
    $this->vista('pages/usuario', $datos);
}      


        /////////////////////////////////////////////////////////////////////LOGIN
        
        //Solicitamos las paginas a usar
        public function login(){
            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['usuario']) && isset($_POST['contrasena'])){
                    //Colocamos los datos recibidos en una matriz para proceder al Modelo
                    $data = [
                        'usuario' => trim($_POST['usuario']),
                        'contrasena' => trim($_POST['contrasena']),
                    ];

                    //Solicitamos a la base de datos el usuario según los datos recibidos de la vista
                    $sesion = $this->loginModelo->getLog($data);

                    //Colocamos el resultado en un arreglo
                    $datos = [
                        'sesion' => $sesion
                    ];

                    //Modificar el almacen segun lo requiera
                    $uactivo = [
                        'usuario' => $datos['sesion']->id,
                        'email' => $datos['sesion']->email,
                        'almacen' => 1
                    ];

                    //Validamos si el usuario y la contraseña son correctos
                    if($datos['sesion']->id != ''){
                        //Insertamos en la tabla de usuarios activos
                        $this->loginModelo->setLogin($uactivo);

                        //Colocamos los datos recibidos en una matriz para proceder al Modelo
                        $data = [
                            'email' => $uactivo['email']
                        ];

                        $UsuarioActivo = $this->loginModelo->getLogActivo($data);

                        //Creamos una variable de sesion usuario
                        $_SESSION['nivel'] = $datos['sesion']->nivel;
                        $_SESSION['usuario'] = $datos['sesion']->username;
                        $_SESSION['id'] = $UsuarioActivo->id;
                        //Creamos una variable de cookie usuario
                        setcookie("nivel", $datos['sesion']->nivel, time()+(60*60*24*30), "/","", 0);
                        setcookie("user", $datos['sesion']->username, time()+(60*60*24*30), "/","", 0);
                        setcookie("id", $datos['sesion']->id, time()+(60*60*24*30), "/","", 0);
                        //Solicitamos a la base de datos el usuario según los datos recibidos de la vista
                        $usuarios = $this->marcaModelo->getMarca();
                        //Colocamos el resultado en un arreglo
                        $datos = [
                            'usuarios' => $usuarios
                        ];
                        $this->vista('pages/start', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    die('Algo salio mal');
                }
            }else{    
                $this->vista('pages/login');
            }
        }


        //////////////////////////////////////////////////////////////CERRAR SESION
        
        //Solicitamos las paginas a usar
        public function cerrar(){
            $id = $_SESSION['id'];

            $data = [
                'id' => $id
            ];

            $this->logoutModelo->deleteLogout($data);
            //Destruimos una variable de sesion usuario
            session_destroy();
            unset($_SESSION['nivel']);
            unset($_SESSION['usuario']);
            unset($_SESSION['nivel']);
            //Destruimos una variable de cookie usuario
            setcookie("user", "" , time()-(60), "/","", 0);
            setcookie("id", "" , time()-(60), "/","", 0);
            setcookie("nivel", "" , time()-(60), "/","", 0);
            $this->vista('pages/login');
        }

        //////////////////////////////////////////////////////////////////////COMPRA

        public function compra(){
            //Solicitamos la funcion para obtener datos de la tabla
            $articulo = $this->articuloModelo->getArticulo();
            $proveedor = $this->proveedorModelo->getProveedor();
            $almacen = $this->almacenModelo->getAlmacen();
            $grupo = $this->grupoModelo->getGrupo();
            $subgrupo = $this->subgrupoModelo->getSubgrupo();
            $marca = $this->marcaModelo->getMarca();
            $unidad = $this->tipoUnidadModelo->getTipoUnidad();
            $tipopago = $this->tipoPagoModelo->getTipoPago();
            $vendedor = $this->empleadoModelo->getEmpleado();
            $municipio = $this->municipioModelo->getMunicipio();
            $estado = $this->estadoModelo->getEstado();

            //Creamos una matriz para colocar los datos obtenidos de la tabla Articulo y sus conexiones
            $datos = [
                'articulo' => $articulo,
                'proveedor' => $proveedor,
                'almacen' => $almacen,
                'grupo' => $grupo,
                'subgrupo' => $subgrupo,
                'marca' => $marca,
                'tipounidad' => $unidad,
                'tipopago' => $tipopago,
                'vendedor' => $vendedor,
                'estado' => $estado,
                'municipio' => $municipio
            ];

            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                if(isset($_POST['proveedor'])){
                    $data = [
                        'usuario' => trim($_POST['formTotal']),
                        'total' => trim($_POST['formTotal']),
                        'fecha' => trim($_POST['fecha']),
                        'proveedor' => trim($_POST['proveedor']),
                        'factura' => trim($_POST['factura']),
                        'tipopago' => trim($_POST['tipopago']),
                        'almacen' => $_POST['IdAlmacenCompraTabla'],
                        'articulo' => $_POST['articuloCompra'],
                        'cantidad' => $_POST['CantidadCompraTabla'],
                        'precio' => $_POST['PrecioCompraTabla'],
                        'inventario' => $_POST['IdInventarioCompraTabla']
                    ];

                    if($this->compraModelo->setCompra($data)){
                        $this->vista('pages/compra', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
                else if(isset($_POST['categoria'])){

                    $data = [
                        'categoria' => trim($_POST['categoria'])
                    ];

                    $articulo = $this->articuloModelo->getArticuloCategoria($data);
                    
                    //Creamos una matriz para colocar los datos obtenidos de la tabla Articulo y sus conexiones
                    $datos = [
                        'articulo' => $articulo,
                        'proveedor' => $proveedor,
                        'almacen' => $almacen,
                        'grupo' => $grupo,
                        'subgrupo' => $subgrupo,
                        'marca' => $marca,
                        'tipounidad' => $unidad
                    ];
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    //Validamos si la consulta se ejecuta correctamente 
                    
                    echo json_encode($datos); 
                }
                else if(isset($_POST['almacen'])){
                    $data = [
                        'articulo' => trim($_POST['articulo']),
                        'almacen' => trim($_POST['almacen']),
                    ];

                    $inventario = $this->inventarioModelo->getCantidadInventario($data);
                    
                    //Creamos una matriz para colocar los datos obtenidos de la tabla Articulo y sus conexiones
                    $datos = [
                        'inventario' => $inventario
                    ];
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    //Validamos si la consulta se ejecuta correctamente 
                    
                    echo json_encode($datos); 
                }
                else if(isset($_POST['articulo'])){

                    $data = [
                        'id' => trim($_POST['articulo'])
                    ];

                    $articulo = $this->articuloModelo->getFindArticulo($data);
                    
                    //Creamos una matriz para colocar los datos obtenidos de la tabla Articulo y sus conexiones
                    $datos = [
                        'articulo' => $articulo
                    ];
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    //Validamos si la consulta se ejecuta correctamente 
                    
                    echo json_encode($datos); 
                }
            }else{
                $data = [
                    'descr' => "",
                ];

                $this->vista('pages/compra', $datos);
            }

            //$this->vista('pages/compra', $datos);
        }


        //////////////////////////////////////////////////////////////////////VENTA

        public function venta(){
            //Solicitamos la funcion para obtener datos de la tabla
            $articulo = $this->articuloModelo->getArticulo();
            $cliente = $this->clienteModelo->getCliente();
            $almacen = $this->almacenModelo->getAlmacen();
            $grupo = $this->grupoModelo->getGrupo();
            $subgrupo = $this->subgrupoModelo->getSubgrupo();
            $marca = $this->marcaModelo->getMarca();
            $unidad = $this->tipoUnidadModelo->getTipoUnidad();
            $tipopago = $this->tipoPagoModelo->getTipoPago();
            $vendedor = $this->empleadoModelo->getEmpleado();
            $municipio = $this->municipioModelo->getMunicipio();
            $estado = $this->estadoModelo->getEstado();

            //Creamos una matriz para colocar los datos obtenidos de la tabla Articulo y sus conexiones
            $datos = [
                'articulo' => $articulo,
                'cliente' => $cliente,
                'almacen' => $almacen,
                'grupo' => $grupo,
                'subgrupo' => $subgrupo,
                'marca' => $marca,
                'tipounidad' => $unidad,
                'tipopago' => $tipopago,
                'vendedor' => $vendedor,
                'municipio' => $municipio,
                'estado' => $estado,
            ];

            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                if(isset($_POST['cliente'])){
                    $data = [
                        'usuario' => trim($_POST['formTotal']),
                        'total' => trim($_POST['formTotal']),
                        'fecha' => trim($_POST['fecha']),
                        'cliente' => trim($_POST['cliente']),
                        'factura' => trim($_POST['factura']),
                        'tipopago' => trim($_POST['tipopago']),
                        'almacen' => $_POST['IdAlmacenVentaTabla'],
                        'articulo' => $_POST['articuloVenta'],
                        'cantidad' => $_POST['CantidadVentaTabla'],
                        'precio' => $_POST['PrecioVentaTabla'],
                        'inventario' => $_POST['IdInventarioVentaTabla']
                    ];

                    if($this->ventaModelo->setVenta($data)){
                        $this->vista('pages/venta', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }
                else if(isset($_POST['categoria'])){

                    $data = [
                        'categoria' => trim($_POST['categoria'])
                    ];

                    $articulo = $this->articuloModelo->getArticuloCategoria($data);
                    
                    //Creamos una matriz para colocar los datos obtenidos de la tabla Articulo y sus conexiones
                    $datos = [
                        'articulo' => $articulo,
                        'cliente' => $cliente,
                        'almacen' => $almacen,
                        'grupo' => $grupo,
                        'subgrupo' => $subgrupo,
                        'marca' => $marca,
                        'tipounidad' => $unidad
                    ];
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    //Validamos si la consulta se ejecuta correctamente 
                    
                    echo json_encode($datos); 
                }
                else if(isset($_POST['almacen'])){
                    $data = [
                        'articulo' => trim($_POST['articulo']),
                        'almacen' => trim($_POST['almacen']),
                    ];

                    $inventario = $this->inventarioModelo->getCantidadInventario($data);
                    
                    //Creamos una matriz para colocar los datos obtenidos de la tabla Articulo y sus conexiones
                    $datos = [
                        'inventario' => $inventario
                    ];
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    //Validamos si la consulta se ejecuta correctamente 
                    
                    echo json_encode($datos); 
                }
                else if(isset($_POST['articulo'])){

                    $data = [
                        'id' => trim($_POST['articulo'])
                    ];

                    $articulo = $this->articuloModelo->getFindArticulo($data);
                    
                    //Creamos una matriz para colocar los datos obtenidos de la tabla Articulo y sus conexiones
                    $datos = [
                        'articulo' => $articulo
                    ];
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    //Validamos si la consulta se ejecuta correctamente 
                    
                    echo json_encode($datos); 
                }
            }else{
                $data = [
                    'usuario' => "",
                        'total' => "",
                        'fecha' => "",
                        'cliente' => "",
                        'factura' => "",
                        'tipopago' => "",
                        'almacen' => "",
                        'articulo' => "",
                        'cantidad' => "",
                        'precio' => "",
                        'inventario' => ""
                ];

                $this->vista('pages/venta', $datos);
            }

            //$this->vista('pages/compra', $datos);
        }
        

        //////////////////////////////////////////////////////////////////////REPORTECOMPRA

        public function reportecompras(){
            //Solicitamos la funcion para obtener datos de la tabla
            $compra = $this->compraModelo->getCompra();

            //Creamos una matriz para colocar los datos obtenidos de la tabla Articulo y sus conexiones
            $datos = [
                'compra' => $compra
            ];

            //Validamos que se realizó una solicitud del metodo Post
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Para realizar una actualización validamos tener los datos a actualizar
                if(isset($_POST['fechainicio']) && isset($_POST['fechafinal'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'fechainicio' => trim($_POST['fechainicio']),
                        'fechafinal' => trim($_POST['fechafinal'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    $reportes = $this->compraModelo->getCompraFecha($data);

                    $datos = [
                        'compra' => $reportes 
                    ];

                    echo json_encode($datos); 
                    
                    //Para realizar una eliminación validamos el id
                }/*else if(isset($_POST['id'])){
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'id' => trim($_POST['id'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->subgrupoModelo->deleteSubgrupo($data)){
                        $this->vista('pages/reportecompras', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }else{
                    //Colocamos los datos obtenidos en una matriz para proceder al Modelo
                    $data = [
                        'descr' => trim($_POST['desc']),
                        'grupo' => trim($_POST['grupo'])
                    ];
                    //Validamos si la consulta se ejecuta correctamente 
                    if($this->subgrupoModelo->setSubgrupo($data)){
                        $this->vista('pages/reportecompras', $datos);
                    }else{
                        die('Algo salio mal');
                    }
                }*/
            }else{

                $this->vista('pages/reportecompras', $datos);
            }
            //$this->vista('pages/compra', $datos);
        }

    }
?>