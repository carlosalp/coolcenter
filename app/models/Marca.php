<?php
    class Marca{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getMarca(){
            $this->db->query("SELECT * FROM `marca` ORDER BY `nombre_marca`;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setMarca($data){
            $this->db->query("INSERT INTO `marca`(`id_marca`, `nombre_marca`) VALUES (NULL,:descr);");
            
            //vincuar valores
            $this->db->bind(':descr', $data['descr']);
            
            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateMarca($data){
            $this->db->query("UPDATE `marca` SET `id_marca`=:id,`nombre_marca`=:descr WHERE `id_marca`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':descr', $data['descr']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteMarca($data){
            $this->db->query("DELETE FROM `marca` WHERE `id_marca`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>