<?php
    class Empleado{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getEmpleado(){
            $this->db->query("SELECT * FROM `persona` AS p INNER JOIN `estado` AS e INNER JOIN `municipio` AS m ON p.`municipio_persona` = m.`id_municipio` AND p.`estado_persona`=e.`id_estado` AND p.`tipo_persona`=3  ORDER BY `nombre_persona` ;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setEmpleado($data){
            $this->db->query("INSERT INTO `persona` (`nombre_persona`, `municipio_persona`, `cp_persona`, `estado_persona`, `telefono_persona`, `rfc_persona`, `email_persona`, `tipo_persona`, `credito_persona`, `descuento`, `direccion_persona`, `celular_persona`) 
            VALUES (:descr,:municipio,:cp,:estado,:telefono,:rfc,:email,3,:credito,:descuento,:direccion,:celular);");
            
            //vincular valores
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':municipio', $data['municipio']);
            $this->db->bind(':cp', $data['cp']);
            $this->db->bind(':estado', $data['estado']);
            $this->db->bind(':telefono', $data['telefono']);
            $this->db->bind(':rfc', $data['rfc']);
            $this->db->bind(':email', $data['email']);
            $this->db->bind(':credito', $data['credito']);
            $this->db->bind(':descuento', $data['descuento']);
            $this->db->bind(':direccion', $data['direccion']);
            $this->db->bind(':celular', $data['celular']);
            
            //Ejecutar
            if($this->db->execute()){
                //Crear la consulta
                $this->db->query("INSERT INTO `members`( `nivel`, `username`, `email`, `password`) 
                VALUES (:nivel,:usuario,:email,:pass);");
                //Vincular vallores
                $this->db->bind(':nivel', $data['nivel']);
                $this->db->bind(':usuario', $data['usuario']);
                $this->db->bind(':email', $data['email']);
                $this->db->bind(':pass', $data['pass']);
                   
                    //Ejecutar
                    if($this->db->execute()){
                        return true;
                    }
                    else{
                        return false;
                    }
            }
            
            else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateEmpleado($data){
            $this->db->query("UPDATE `persona` SET `nombre_persona`=:descr,`municipio_persona`=:municipio,
            `cp_persona`=:cp,`estado_persona`=:estado,`telefono_persona`=:telefono,`rfc_persona`=:rfc,
            `email_persona`=:email,`tipo_persona`=3,`credito_persona`=:credito,`descuento`=:descuento,`direccion_persona`=:direccion,`celular_persona`=:celular 
            WHERE `id_persona`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':municipio', $data['municipio']);
            $this->db->bind(':cp', $data['cp']);
            $this->db->bind(':estado', $data['estado']);
            $this->db->bind(':telefono', $data['telefono']);
            $this->db->bind(':rfc', $data['rfc']);
            $this->db->bind(':email', $data['email']);
            $this->db->bind(':credito', $data['credito']);
            $this->db->bind(':descuento', $data['descuento']);
            $this->db->bind(':direccion', $data['direccion']);
            $this->db->bind(':celular', $data['celular']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteEmpleado($data){
            $this->db->query("DELETE FROM `persona` WHERE `id_persona`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            //Ejecutar
            if($this->db->execute()){
                $this->db->query("DELETE FROM `members` WHERE `email`=:email;");
                //return $this->db->registers();
    
                //vincuar valores
                $this->db->bind(':email', $data['email']);
                    //Ejecutar
                if($this->db->execute()){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

    }
?>