<?php
    class Estado{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getEstado(){
            $this->db->query("SELECT * FROM `estado` ORDER BY `nombre_estado`;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setEstado($data){
            $this->db->query("INSERT INTO `estado`( `nombre_estado`) VALUES (:descr);");
            
            //vincular valores
            $this->db->bind(':descr', $data['descr']);
            
            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateEstado($data){
            $this->db->query("UPDATE `estado` SET `nombre_estado`=:descr WHERE `id_estado`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':descr', $data['descr']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteEstado($data){
            $this->db->query("DELETE FROM `estado` WHERE `id_estado`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>