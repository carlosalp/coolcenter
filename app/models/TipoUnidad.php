<?php
    class TipoUnidad{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getTipoUnidad(){
            $this->db->query("SELECT * FROM `tipo_unidad` ORDER BY `nombre_tipo_unidad`;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setTipoUnidad($data){
            $this->db->query("INSERT INTO `tipo_unidad`(`id_tipo_unidad`, `nombre_tipo_unidad`) VALUES (NULL,:nombre);");
            
            //vincuar valores
            $this->db->bind(':nombre', $data['nombre']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateTipoUnidad($data){
            $this->db->query("UPDATE `tipo_unidad` SET `id_tipo_unidad`=:id,`nombre_tipo_unidad`=:nombre WHERE `id_tipo_unidad`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':nombre', $data['nombre']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteTipoUnidad($data){
            $this->db->query("DELETE FROM `tipo_unidad` WHERE `id_tipo_unidad`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>