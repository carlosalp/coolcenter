<?php
    class Cliente{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getCliente(){
            $this->db->query("SELECT * FROM `persona` AS p INNER JOIN `estado` AS e ON p.`estado_persona`=e.`id_estado` INNER JOIN `municipio` AS m ON p.`municipio_persona`=m.`id_municipio` WHERE tipo_persona='1' ORDER BY `nombre_persona` ;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setCliente($data){
            $this->db->query("INSERT INTO `persona` (`nombre_persona`, `municipio_persona`, `cp_persona`, `estado_persona`, `telefono_persona`, `rfc_persona`, `email_persona`, `tipo_persona`, `credito_persona`, `descuento`, `direccion_persona`, `celular_persona`) 
            VALUES (:descr,:municipio,:cp,:estado,:telefono,:rfc,:email,1,:credito,:descuento,:direccion,:celular);");
            
            //vincular valores
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':municipio', $data['municipio']);
            $this->db->bind(':cp', $data['cp']);
            $this->db->bind(':estado', $data['estado']);
            $this->db->bind(':telefono', $data['telefono']);
            $this->db->bind(':rfc', $data['rfc']);
            $this->db->bind(':email', $data['email']);
            $this->db->bind(':credito', $data['credito']);
            $this->db->bind(':descuento', $data['descuento']);
            $this->db->bind(':direccion', $data['direccion']);
            $this->db->bind(':celular', $data['celular']);
            
            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateCliente($data){
            $this->db->query("UPDATE `persona` SET `nombre_persona`=:descr,`municipio_persona`=:municipio,
            `cp_persona`=:cp,`estado_persona`=:estado,`telefono_persona`=:telefono,`rfc_persona`=:rfc,
            `email_persona`=:email,`tipo_persona`=1,`credito_persona`=:credito,`descuento`=:descuento,`direccion_persona`=:direccion,`celular_persona`=:celular 
            WHERE `id_persona`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':municipio', $data['municipio']);
            $this->db->bind(':cp', $data['cp']);
            $this->db->bind(':estado', $data['estado']);
            $this->db->bind(':telefono', $data['telefono']);
            $this->db->bind(':rfc', $data['rfc']);
            $this->db->bind(':email', $data['email']);
            $this->db->bind(':credito', $data['credito']);
            $this->db->bind(':descuento', $data['descuento']);
            $this->db->bind(':direccion', $data['direccion']);
            $this->db->bind(':celular', $data['celular']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteCliente($data){
            $this->db->query("DELETE FROM `persona` WHERE `id_persona`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>