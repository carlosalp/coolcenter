<?php
    class Compra{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        public function getCompra(){
            $this->db->query("SELECT dc.`id_compra`, dc.`factura`, dc.`id_usuario`, (SELECT p.`nombre_persona` FROM `persona` AS p WHERE p.`id_persona` = dc.`id_usuario`) vendedor, 
            dc.`total_compra`, dc.`fecha_compra`, dc.`tipo_pago`, tp.`nombre_tipo_pago`, dc.`id_proveedor`, (SELECT p.`nombre_persona` FROM `persona` AS p WHERE p.`id_persona` = dc.`id_proveedor`) proveedor 
            FROM `detalle_compra` AS dc INNER JOIN `tipo_pago` AS tp ON tp.`id_tipo_pago` = dc.`tipo_pago`;");
            return $this->db->registers();
        }

        public function getCompraFecha($data){
            $this->db->query("SELECT dc.`id_compra`, dc.`factura`, dc.`id_usuario`, (SELECT p.`nombre_persona` FROM `persona` AS p WHERE p.`id_persona` = dc.`id_usuario`) vendedor, 
            dc.`total_compra`, dc.`fecha_compra`, dc.`tipo_pago`, tp.`nombre_tipo_pago`, dc.`id_proveedor`, (SELECT p.`nombre_persona` FROM `persona` AS p WHERE p.`id_persona` = dc.`id_proveedor`) proveedor 
            FROM `detalle_compra` AS dc INNER JOIN `tipo_pago` AS tp ON tp.`id_tipo_pago` = dc.`tipo_pago` AND `fecha_compra` BETWEEN :fechainicio AND :fechafinal;");

            $this->db->bind(':fechainicio', $data['fechainicio']);
            $this->db->bind(':fechafinal', $data['fechafinal']);

            return $this->db->registers();
        }

        public function setCompra($data){
            //Crear la consulta
            $this->db->query("INSERT INTO `detalle_compra`(`id_compra`, `factura`, `id_usuario`, `total_compra`, `fecha_compra`, `tipo_pago`, `id_proveedor`) VALUES 
            (NULL,:factura,:usuario,:total,:fecha,:tipopago,:proveedor);");

            //Vincular vallores
            $this->db->bind(':factura', $data['factura']);
            $this->db->bind(':usuario', $data['usuario']);
            $this->db->bind(':total', $data['total']);
            $this->db->bind(':fecha', $data['fecha']);
            $this->db->bind(':tipopago', $data['tipopago']);
            $this->db->bind(':proveedor', $data['proveedor']);

            //Ejecutar
            if($this->db->execute()){
                $respuesta=true;
                $numregistros = 0;
                while ($numregistros < count($data['articulo']))
                {   
                    //Crear la consulta
                    $this->db->query("INSERT INTO `compras`(`id_compras`, `id_articulo`, `factura`, `cantidad_producto`, `precio_compra`, `id_almacen`) VALUES (NULL, :articulo, :factura, :cantidad, :precio, :almacen);");
                    //Vincular vallores
                    $this->db->bind(':factura', $data['factura']);
                    $this->db->bind(':articulo', $data['articulo'][$numregistros]);
                    $this->db->bind(':cantidad', $data['cantidad'][$numregistros]);
                    $this->db->bind(':precio', $data['precio'][$numregistros]);
                    $this->db->bind(':almacen', $data['almacen'][$numregistros]);
                    //Ejecutar
                    if($this->db->execute() != true){
                        $respuesta = false;
                        break;
                    }
                    $numregistros++;
                }
                //Ejecutar
                if($respuesta){
                    $respuesta=true;
                    $numregistros = 0;
                    while ($numregistros < count($data['articulo']))
                    {
                        if($data['inventario'][$numregistros] == ""){
                            //Crear la consulta
                            $this->db->query("INSERT INTO `inventario`(`id_inventario`, `id_articulo`, `cantidad_articulo`, `id_almacen`) VALUES (NULL, :articulo, :cantidad, :almacen);");
                            //Vincular vallores
                            $this->db->bind(':articulo', $data['articulo'][$numregistros]);
                            $this->db->bind(':cantidad', $data['cantidad'][$numregistros]);
                            $this->db->bind(':almacen', $data['almacen'][$numregistros]);
                        }
                        else{
                            //Crear la consulta
                            $this->db->query("UPDATE `inventario` SET `id_inventario` = :inventario, `id_articulo` = :articulo, `cantidad_articulo` = `cantidad_articulo`+:cantidad, 
                            `id_almacen` = :almacen WHERE id_inventario = :inventario;");
                            //Vincular vallores
                            $this->db->bind(':inventario', $data['inventario'][$numregistros]);
                            $this->db->bind(':articulo', $data['articulo'][$numregistros]);
                            $this->db->bind(':cantidad', $data['cantidad'][$numregistros]);
                            $this->db->bind(':almacen', $data['almacen'][$numregistros]);
                        }
                        
                        
                        //Ejecutar
                        if(!$this->db->execute()){
                            $respuesta = false;
                            break;
                        }
                        $numregistros++;

                    }    
                    //Ejecutar
                    if($respuesta){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        /*
        public function getArticuloCompra($data){
            $this->db->query("SELECT a.`id_articulo`, a.`nombre_articulo`, a.`ns_articulo`, a.`articulo_descripcion`, a.`articulo_modelo`, 
            a.`precio_venta`, a.`id_subgrupo`, a.`id_grupo`, a.`id_marca`, a.`precio_compra`, a.`descuento`, a.`unidad`, a.`ruta_imagen`,
            g.`nombre_grupo`, s.`nombre_subgrupo`, m.`nombre_marca`, t.`nombre_tipo_unidad`, i.`id_inventario`, i.`cantidad_articulo` FROM `articulo` AS a INNER JOIN `grupo` AS g 
            INNER JOIN `subgrupo` AS s INNER JOIN `marca` AS m INNER JOIN `tipo_unidad` AS t INNER JOIN `inventario` AS i ON a.`id_grupo` = g.`id_grupo` AND 
            a.`id_subgrupo` = s.`id_subgrupo` AND a.`id_marca` = m.`id_marca` AND a.`unidad` = t.`id_tipo_unidad` AND i.`id_articulo` = a.`id_articulo` AND a.`id_grupo` = :categoria ORDER BY `nombre_articulo`;");
            
            $this->db->bind(':categoria', $data['categoria']);
            
            return $this->db->registers();
        }*/

        //Funcion para obtener los datos de tabla
        /*public function getCompra(){
            $this->db->query("SELECT a.`id_articulo`, a.`nombre_articulo`, a.`ns_articulo`, a.`articulo_descripcion`, a.`articulo_modelo`, 
            a.`precio_venta`, a.`id_subgrupo`, a.`id_grupo`, a.`id_marca`, a.`precio_compra`, a.`descuento`, a.`unidad`, a.`ruta_imagen`,
            g.`nombre_grupo`, s.`nombre_subgrupo`, m.`nombre_marca`, t.`nombre_tipo_unidad` FROM `articulo` AS a INNER JOIN `grupo` AS g 
            INNER JOIN `subgrupo` AS s INNER JOIN `marca` AS m INNER JOIN `tipo_unidad` AS t ON a.`id_grupo` = g.`id_grupo` AND 
            a.`id_subgrupo` = s.`id_subgrupo` AND a.`id_marca` = m.`id_marca` AND a.`unidad` = t.`id_tipo_unidad` ORDER BY `nombre_articulo`;");
            return $this->db->registers();
        }*/

        //Obtenemos el articulo con la id obtenida
        /*public function getFindArticulo(){
            $this->db->query("SELECT * FROM `articulo` WHERE `id_articulo` = :id");
            
            $this->db->bind(':id', $data['id']);
            
            return $this->db->register();
        }

        //Obtenemos el numero de registros de la tabla
        public function getArticuloNums(){
            $this->db->query("SELECT * FROM `articulo`;");
            $this->db->execute();
            return $this->db->rowCount();
        }
        */
        /*
        //Funcion para insertar datos en la tabla
        public function setArticulo($data){
            $this->db->query("INSERT INTO `compras`(`id_articulo`, `nombre_articulo`, `ns_articulo`, `articulo_descripcion`,
            `articulo_modelo`, `precio_venta`,`id_subgrupo`,`id_grupo`,`id_marca`,`precio_compra`,`descuento`, `unidad`, `ruta_imagen`) 
            VALUES (NULL,:nombre,:ns,:descr,:modelo,:precio_venta,:subgrupo,:grupo,:marca,0,:descuento,:unidad,:img);");
            
            //vincuar valores
            $this->db->bind(':nombre', $data['nombre']);
            $this->db->bind(':ns', $data['ns']);
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':modelo', $data['modelo']);
            $this->db->bind(':precio_venta', $data['precio_venta']);
            $this->db->bind(':subgrupo', $data['subgrupo']);
            $this->db->bind(':grupo', $data['grupo']);
            $this->db->bind(':marca', $data['marca']);
            //$this->db->bind(':precio_compra', $data['precio_compra']);
            $this->db->bind(':descuento', $data['descuento']);
            $this->db->bind(':unidad', $data['unidad']);
            $this->db->bind(':img', $data['img']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateArticulo($data){
            $this->db->query("UPDATE `articulo` SET `id_articulo`=:id,`nombre_articulo`=:nombre, `ns_articulo`=:ns,`articulo_descripcion`=:descr,
            `articulo_modelo`=:modelo,`precio_venta`=:precio_venta, `id_subgrupo`=:subgrupo, `id_grupo`=:grupo,`id_marca`=:marca, 
            `descuento`=:descuento, `unidad`=:unidad, `ruta_imagen`=:img WHERE `id_articulo`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':nombre', $data['nombre']);
            $this->db->bind(':ns', $data['ns']);
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':modelo', $data['modelo']);
            $this->db->bind(':precio_venta', $data['precio_venta']);
            $this->db->bind(':subgrupo', $data['subgrupo']);
            $this->db->bind(':grupo', $data['grupo']);
            $this->db->bind(':marca', $data['marca']);
            //$this->db->bind(':precio_compra', $data['precio_compra']);
            $this->db->bind(':descuento', $data['descuento']);
            $this->db->bind(':unidad', $data['unidad']);
            $this->db->bind(':img', $data['img']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteArticulo($data){
            $this->db->query("DELETE FROM `articulo` WHERE `id_articulo`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        */
    }
?>