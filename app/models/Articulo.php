<?php
    class Articulo{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getArticulo(){
            $this->db->query("SELECT a.`id_articulo`, a.`nombre_articulo`, a.`ns_articulo`, a.`articulo_descripcion`, a.`articulo_modelo`, 
            a.`precio_venta`, a.`id_subgrupo`, a.`id_grupo`, a.`id_marca`, a.`precio_compra`, a.`descuento`, a.`unidad`, a.`ruta_imagen`,
            g.`nombre_grupo`, s.`nombre_subgrupo`, m.`nombre_marca`, t.`nombre_tipo_unidad` FROM `articulo` AS a INNER JOIN `grupo` AS g 
            INNER JOIN `subgrupo` AS s INNER JOIN `marca` AS m INNER JOIN `tipo_unidad` AS t ON a.`id_grupo` = g.`id_grupo` AND 
            a.`id_subgrupo` = s.`id_subgrupo` AND a.`id_marca` = m.`id_marca` AND a.`unidad` = t.`id_tipo_unidad` ORDER BY `nombre_articulo`;");
            return $this->db->registers();
        }

        //Funcion para obtener los datos de tabla segun su categoria
        public function getArticuloCategoria($data){
            $this->db->query("SELECT a.`id_articulo`, a.`nombre_articulo`, a.`ns_articulo`, a.`articulo_descripcion`, a.`articulo_modelo`, 
            a.`precio_venta`, a.`id_subgrupo`, a.`id_grupo`, a.`id_marca`, a.`precio_compra`, a.`descuento`, a.`unidad`, a.`ruta_imagen`,
            g.`nombre_grupo`, s.`nombre_subgrupo`, m.`nombre_marca`, t.`nombre_tipo_unidad` FROM `articulo` AS a INNER JOIN `grupo` AS g 
            INNER JOIN `subgrupo` AS s INNER JOIN `marca` AS m INNER JOIN `tipo_unidad` AS t ON a.`id_grupo` = g.`id_grupo` AND 
            a.`id_subgrupo` = s.`id_subgrupo` AND a.`id_marca` = m.`id_marca` AND a.`unidad` = t.`id_tipo_unidad` AND g.`id_grupo` = :categoria ORDER BY `nombre_articulo`;");

            //vincular los datos recibidos
            $this->db->bind(':categoria', $data['categoria']);

            return $this->db->registers();
        }

        //Obtenemos el articulo con la id obtenida
        public function getFindArticulo($data){
            $this->db->query("SELECT a.`id_articulo`, a.`nombre_articulo`, a.`ns_articulo`, a.`articulo_descripcion`, a.`articulo_modelo`, 
            a.`precio_venta`, a.`id_subgrupo`, a.`id_grupo`, a.`id_marca`, a.`precio_compra`, a.`descuento`, a.`unidad`, a.`ruta_imagen`,
            g.`nombre_grupo`, s.`nombre_subgrupo`, m.`nombre_marca`, t.`nombre_tipo_unidad` FROM `articulo` AS a INNER JOIN `grupo` AS g 
            INNER JOIN `subgrupo` AS s INNER JOIN `marca` AS m INNER JOIN `tipo_unidad` AS t ON a.`id_grupo` = g.`id_grupo` AND 
            a.`id_subgrupo` = s.`id_subgrupo` AND a.`id_marca` = m.`id_marca` AND a.`unidad` = t.`id_tipo_unidad` AND `id_articulo` = :id ORDER BY `nombre_articulo`");
            
            $this->db->bind(':id', $data['id']);
            
            return $this->db->register();
        }

        //Obtenemos el numero de registros de la tabla
        public function getArticuloNums(){
            $this->db->query("SELECT * FROM `articulo`;");
            $this->db->execute();
            return $this->db->rowCount();
        }
        
        //Funcion para insertar datos en la tabla
        public function setArticulo($data){
            $this->db->query("INSERT INTO `articulo`(`id_articulo`, `nombre_articulo`, `ns_articulo`, `articulo_descripcion`,
             `articulo_modelo`, `precio_venta`,`id_subgrupo`,`id_grupo`,`id_marca`,`precio_compra`,`descuento`, `unidad`, `ruta_imagen`) 
             VALUES (NULL,:nombre,:ns,:descr,:modelo,:precio_venta,:subgrupo,:grupo,:marca,0,:descuento,:unidad,:img);");
            
            //vincuar valores
            $this->db->bind(':nombre', $data['nombre']);
            $this->db->bind(':ns', $data['ns']);
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':modelo', $data['modelo']);
            $this->db->bind(':precio_venta', $data['precio_venta']);
            $this->db->bind(':subgrupo', $data['subgrupo']);
            $this->db->bind(':grupo', $data['grupo']);
            $this->db->bind(':marca', $data['marca']);
            //$this->db->bind(':precio_compra', $data['precio_compra']);
            $this->db->bind(':descuento', $data['descuento']);
            $this->db->bind(':unidad', $data['unidad']);
            $this->db->bind(':img', $data['img']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateArticulo($data){
            $this->db->query("UPDATE `articulo` SET `id_articulo`=:id,`nombre_articulo`=:nombre, `ns_articulo`=:ns,`articulo_descripcion`=:descr,
            `articulo_modelo`=:modelo,`precio_venta`=:precio_venta, `id_subgrupo`=:subgrupo, `id_grupo`=:grupo,`id_marca`=:marca, 
            `descuento`=:descuento, `unidad`=:unidad, `ruta_imagen`=:img WHERE `id_articulo`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':nombre', $data['nombre']);
            $this->db->bind(':ns', $data['ns']);
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':modelo', $data['modelo']);
            $this->db->bind(':precio_venta', $data['precio_venta']);
            $this->db->bind(':subgrupo', $data['subgrupo']);
            $this->db->bind(':grupo', $data['grupo']);
            $this->db->bind(':marca', $data['marca']);
            //$this->db->bind(':precio_compra', $data['precio_compra']);
            $this->db->bind(':descuento', $data['descuento']);
            $this->db->bind(':unidad', $data['unidad']);
            $this->db->bind(':img', $data['img']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteArticulo($data){
            $this->db->query("DELETE FROM `articulo` WHERE `id_articulo`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>