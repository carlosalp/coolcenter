<?php
    class Login{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getLog($data){
            $this->db->query("SELECT * FROM `members` WHERE `username` = :usuario OR `email` = :usuario AND `password` = :contrasena;");

            //vincular valores
            $this->db->bind(':usuario', $data['usuario']);
            $this->db->bind(':contrasena', $data['contrasena']);

            return $this->db->register();
        }

        public function getLogActivo($data){
            $this->db->query("SELECT * FROM `uactivos` WHERE `email` = :email;");

            //vincular valores
            $this->db->bind(':email', $data['email']);

            return $this->db->register();
        }

        //Funcion para insertar datos en la tabla
        public function setLogin($data){
            $this->db->query("INSERT INTO `uactivos`(`id`, `email`,`usuario`,`aalmacen`) VALUES (NULL,:email,:usuario,:almacen);");
            
            //vincuar valores
            $this->db->bind(':usuario', $data['usuario']);
            $this->db->bind(':email', $data['email']);
            $this->db->bind(':almacen', $data['almacen']);
            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>