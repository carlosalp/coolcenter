<?php
    class Municipio{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getMunicipio(){
            $this->db->query("SELECT * FROM `municipio` AS m INNER JOIN `estado` AS e ON e.`id_estado`=m.`id_estado`  ORDER BY `nombre_municipio`;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setMunicipio($data){
            $this->db->query("INSERT INTO `municipio`(`nombre_municipio`, `id_estado`) VALUES (:descr,:estado);");
            
            //vincular valores
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':estado', $data['estado']);
            
            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateMunicipio($data){
            $this->db->query("UPDATE `municipio` SET `nombre_municipio`=:descr,`id_estado`=:estado where `id_municipio`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':estado', $data['estado']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteMunicipio($data){
            $this->db->query("DELETE FROM `municipio` WHERE `id_municipio`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>