<?php
    class Subgrupo{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getSubgrupo(){
            $this->db->query("SELECT * FROM `subgrupo` AS s INNER JOIN `grupo` AS g ON s.id_grupo = g.id_grupo ORDER BY `nombre_subgrupo`;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setSubgrupo($data){
            $this->db->query("INSERT INTO `subgrupo`(`id_subgrupo`, `nombre_subgrupo`, `id_grupo`) VALUES (NULL,:descr,:grupo);");
            
            //vincuar valores
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':grupo', $data['grupo']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateSubgrupo($data){
            $this->db->query("UPDATE `subgrupo` SET `id_subgrupo`=:id,`nombre_subgrupo`=:descr, `id_grupo`=:grupo WHERE `id_subgrupo`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':grupo', $data['grupo']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteSubgrupo($data){
            $this->db->query("DELETE FROM `subgrupo` WHERE `id_subgrupo`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>