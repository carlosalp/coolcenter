<?php
    class Almacen{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getAlmacen(){
            $this->db->query("SELECT * FROM `almacen` ORDER BY `nombre_almacen`;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setAlmacen($data){
            $this->db->query("INSERT INTO `almacen`( `nombre_almacen`, `direccion_almacen`, `telefono_almacen`) 
            VALUES (:descr,:direccion,:telefono);");
            
            //vincuar valores
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':direccion', $data['direccion']);
            $this->db->bind(':telefono', $data['telefono']);
            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateAlmacen($data){
            $this->db->query("UPDATE `almacen` SET `nombre_almacen`=[value-2],`direccion_almacen`=[value-3],`telefono_almacen`=[value-4] WHERE 1;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':descr', $data['descr']);
            $this->db->bind(':direccion', $data['direccion']);
            $this->db->bind(':telefono', $data['telefono']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteAlmacen($data){
            $this->db->query("DELETE FROM `almacen` WHERE `id_almacen`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>