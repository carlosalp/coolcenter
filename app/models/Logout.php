<?php
    class Logout{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteLogout($data){
            $this->db->query("DELETE FROM `uactivos` WHERE `id`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>