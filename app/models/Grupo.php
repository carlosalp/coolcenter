<?php
    class Grupo{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getGrupo(){
            $this->db->query("SELECT * FROM `grupo` ORDER BY `nombre_grupo`;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setGrupo($data){
            $this->db->query("INSERT INTO `grupo`(`id_grupo`, `nombre_grupo`) VALUES (NULL,:descr);");
            
            //vincular valores
            $this->db->bind(':descr', $data['descr']);
            
            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateGrupo($data){
            $this->db->query("UPDATE `grupo` SET `id_grupo`=:id,`nombre_grupo`=:descr WHERE `id_grupo`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':descr', $data['descr']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteGrupo($data){
            $this->db->query("DELETE FROM `grupo` WHERE `id_grupo`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>