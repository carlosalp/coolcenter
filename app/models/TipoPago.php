<?php
    class TipoPago{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getTipoPago(){
            $this->db->query("SELECT * FROM `tipo_pago` ORDER BY `nombre_tipo_pago`;");
            return $this->db->registers();
        }
        
        //Funcion para insertar datos en la tabla
        public function setTipoPago($data){
            $this->db->query("INSERT INTO `tipo_pago`(`nombre_tipo_pago`) VALUES (:descr);");
            
            //vincuar valores
            $this->db->bind(':descr', $data['descr']);
            
            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateTipoPago($data){
            $this->db->query("UPDATE `tipo_pago` SET `nombre_tipo_pago`=:descr WHERE `id_tipo_pago`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':descr', $data['descr']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteTipoPago($data){
            $this->db->query("DELETE FROM `tipo_pago` WHERE `id_tipo_pago`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>