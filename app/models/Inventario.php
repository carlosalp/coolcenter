<?php
    class Inventario{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //Funcion para obtener los datos de tabla
        public function getInventario(){
            $this->db->query("SELECT * FROM `inventario` AS i INNER JOIN `almacen` AS a ON i.id_almacen = a.id_almacen INNER JOIN `articulo` AS art ON i.id_articulo = art.id_articulo ORDER BY a.id_almacen ;");
            return $this->db->registers();
        }

        //Funcion para obtener los datos de tabla
        public function getCantidadInventario($data){
            $this->db->query("SELECT * FROM `inventario` WHERE id_almacen = :almacen AND id_articulo = :articulo;");
            //vincular valores
            $this->db->bind(':articulo', $data['articulo']);
            $this->db->bind(':almacen', $data['almacen']);
            return $this->db->register();
        }
        
        //Funcion para insertar datos en la tabla
        public function setInventario($data){
            $this->db->query("INSERT INTO `inventario`(`id_articulo`, `cantidad_articulo`, `id_almacen`) VALUES (:articulo,:cantidad,:almacen);");
            
            //vincuar valores
            $this->db->bind(':articulo', $data['articulo']);
            $this->db->bind(':cantidad', $data['cantidad']);
            $this->db->bind(':almacen', $data['almacen']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para actualizar los valores de la tabla
        public function updateInventario($data){
            $this->db->query("UPDATE `inventario` SET `id_articulo`=:articulo,`cantidad_articulo`=:cantidad,`id_almacen`=:almacen WHERE `id_inventario`=:id;");

            //vincuar valores
            $this->db->bind(':id', $data['id']);
            $this->db->bind(':articulo', $data['articulo']);
            $this->db->bind(':cantidad', $data['cantidad']);
            $this->db->bind(':almacen', $data['almacen']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //Funcion para eliminar los datos seleccionados de la tabla
        public function deleteInventario($data){
            $this->db->query("DELETE FROM `inventario` WHERE `id_inventario`=:id;");
            //return $this->db->registers();

            //vincuar valores
            $this->db->bind(':id', $data['id']);

            //Ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

    }
?>