<?php
    //session_start();
    //if (isset($_SESSION['usuario']) && isset($_COOKIE['user'])){
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php echo NOMBRESITIO; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
        <!-- DataTables-->
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>-->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo RUTA_URL?>/css/DataTables/datatables.min.css"/>-->
        <!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"/>-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css"/>
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"/>-->
        <!-- Bootstrap core CSS -->
        <!--<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">-->
        <link href="<?php echo RUTA_URL?>/css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <!--<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.5/css/mdb.min.css" rel="stylesheet">-->
        <link href="<?php echo RUTA_URL?>/css/mdb.min.css" rel="stylesheet">

        <!--Botones  de exportacion para las datatables-->
        <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="<?php echo RUTA_URL?>/css/estilossidebar.css">

        <link rel="stylesheet" href="<?php echo RUTA_URL?>/css/estiloscards.css">

        <!--Estilos-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo RUTA_URL?>/css/estilos.css">
    </head>
    <body>
        <div>
        <!--<div>-->
            <div class="wrapper">
            <!-- Sidebar Holder -->
                <nav id="sidebar">
                    <div class="sidebar-header">
                        <h3>COOLCENTER</h3>
                    </div>

                    <ul class="list-unstyled components">
                        <p>Sistema Empresarial</p>
                        <li class="active">
                            <a href="<?php echo RUTA_URL?>/pages/start"><i class="fas fa-tachometer-alt "></i> Inicio</a>
                        </li>
                        <li>
                            <a href="<?php echo RUTA_URL?>/pages/venta"><i class="fas fa-dollar-sign"></i> Ventas</a>
                        </li>
                        <li>
                            <a href="<?php echo RUTA_URL?>/pages/compra"><i class="fas fa-shopping-bag"></i> Compras</a>
                        </li>
                    <?php if($_COOKIE['nivel'] == 1){ ?>    
                        <li>
                            <a href="<?php echo RUTA_URL?>/pages/inventario"><i class="fas fa-boxes  "></i> Inventario</a>
                        </li>
                        <li>
                            <a href="#pageSubmenuReportes" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-file"></i> Reportes</a>
                                <ul class="collapse list-unstyled" id="pageSubmenuReportes">
                                    <li>
                                        <a href="<?php echo RUTA_URL?>/pages/reportecompras">Reporte de Compras</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo RUTA_URL?>/pages/reporteventas">Reporte de Ventas</a>
                                    </li>
                                </ul>
                        </li>
                        <li>
                        <!--<a href="<?php echo RUTA_URL?>/pages/articulo"><i class="fas fa-cart-plus"></i> Articulo</a>-->
                            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-cart-plus"></i> Articulo</a>
                                <ul class="collapse list-unstyled" id="pageSubmenu">
                                    <li>
                                        <a href="<?php echo RUTA_URL?>/pages/articulo">Articulo</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo RUTA_URL?>/pages/tipounidad">Tipo de Unidad</a>
                                    </li>
                                </ul>
                        </li>
                        <li>
                            <a href="#pageSubmenuCategoria" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-layer-group"></i> Categorias</a>
                            <ul class="collapse list-unstyled" id="pageSubmenuCategoria">
                                <li>
                                    <a href="<?php echo RUTA_URL?>/pages/marca">Marca</a>
                                </li>
                                <li>
                                    <a href="<?php echo RUTA_URL?>/pages/grupo">Categoria</a>
                                </li>
                                <li>
                                    <a href="<?php echo RUTA_URL?>/pages/subgrupo">Subcategoria</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#pageSubmenuPersona" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-users"></i> Personas</a>
                            <ul class="collapse list-unstyled" id="pageSubmenuPersona">
                                <li>
                                    <a href="<?php echo RUTA_URL?>/pages/proveedor">Proveedores</a>
                                </li>
                                <li>
                                    <a href="<?php echo RUTA_URL?>/pages/cliente">Clientes</a>
                                </li>
                                <li>
                                    <a href="<?php echo RUTA_URL?>/pages/empleado">Empleados</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#pageSubmenuLocalidad" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-globe-americas"></i> Localidad</a>
                            <ul class="collapse list-unstyled" id="pageSubmenuLocalidad">
                                <li>
                                    <a href="<?php echo RUTA_URL?>/pages/estado">Estado</a>
                                </li>
                                <li>
                                    <a href="<?php echo RUTA_URL?>/pages/municipio">Municipio</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo RUTA_URL?>/pages/usuario"><i class="fas fa-user"></i> Usuarios</a>
                        </li>
                        <li>
                            <a href="<?php echo RUTA_URL?>/pages/almacen"><i class="fas fa-warehouse"></i> Almacen</a>
                        </li>
                        <li>
                            <a href="<?php echo RUTA_URL?>/pages/tipopago"><i class="fas fa-credit-card "></i> Tipo Pago</a>
                        </li>
                        <li>
                            <a href="<?php echo RUTA_URL?>/pages/proveedor"><i class="fas fa-truck"></i> Proveedor</a>
                        </li>
                    <?php } ?> 
                    </ul>

                    <!--<ul class="list-unstyled CTAs">
                        <li>
                            <a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>
                        </li>
                        <li>
                            <a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a>
                        </li>
                    </ul>-->
                </nav>

        <!-- Page Content Holder -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-dark ">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?php echo RUTA_URL?>/pages/compra">Compras</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="<?php echo RUTA_URL?>/pages/venta">Ventas</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i><?php echo $_COOKIE['user']?></a>
                                <div class="dropdown-menu active">
                                    <a class="dropdown-item" href="<?php echo RUTA_URL?>/pages/cerrar">Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
<?php require_once RUTA_APP . '/views/modals/modalDelete.php'; ?>
<?php require_once RUTA_APP . '/views/inc/success.php'; ?>
<?php require_once RUTA_APP . '/views/inc/error.php'; ?>

    
