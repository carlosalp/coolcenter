<?php
    //session_start();
    //if (!isset($_SESSION['usuario']) && !isset($_COOKIE['usuario'])){
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php echo NOMBRESITIO; ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
        <!-- DataTables-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css"/>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo RUTA_URL?>/css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="<?php echo RUTA_URL?>/css/mdb.min.css" rel="stylesheet">
        <!-- Estilos SideBar -->
        <link rel="stylesheet" href="<?php echo RUTA_URL?>/css/estilossidebar.css">
        <!--Estilos-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo RUTA_URL?>/css/estilos.css">
    </head>
    <body class="login" style="background-image: url('<?php echo RUTA_URL ?>/imagenes/img/WallpaperLogin.png')">
        <!--<div>-->
            <div class="wrapper">
            <?php require_once RUTA_APP . '/views/inc/error.php'; ?>