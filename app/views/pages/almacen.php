<?php require_once RUTA_APP . '/views/inc/header.php'; ?>
            <div class="view view-cascade gradient-card-header blue lighten-1 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                <div></div>
                <a href="#" class="white-text mx-3">ALMACENES</a>
                <div></div>
            </div>
            <div class="container">
                <div>
                    <a id="modalActivate" data-toggle="modal" data-target="#InsertModal" class="btn light-blue darken-3 waves-effect"><i class="far fa-plus-square" aria-hidden="true"></i> AGREGAR</a>
                </div>
                <?php require_once RUTA_APP . '/views/modals/modalInsert.php'; ?>
                    <form class="text-center p-5" id="InsertMarca" method="POST">
                        <p class="h4 mb-4">NUEVO ALMACEN</p>
                        <div class="md-form form-sm">
                            <input type="text" name="desc" id="desc" class="form-control" required>
                            <label for="form8" class="active">Nombre del almacen:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="direccion" id="direccion" class="form-control" required>
                            <label for="form8" class="active">Direccion del almacen:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="telefono" id="telefono" class="form-control" required>
                            <label for="form8" class="active">Telefono del almacen:</label>
                        </div>
                        <button class="btn btn-outline-primary btn-block" type="submit">Enviar</button>
                    </form>
                <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                <br />
                <div class="table-responsive">
                    <?php require_once RUTA_APP . '/views/datatables/dthead.php'; ?>
                        <thead>
                            <tr>
                                <th class="th-sm">NOMBRE
                                </th>
                                <th class="th-sm">DIRECCION
                                </th>
                                <th class="th-sm">TELEFONO
                                </th>
                                <th class="th-sm">ACCIONES
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach ($datos['almacen'] as $almacen) : ?>
                            <tr>
                                <td id="desc<?php echo $almacen->id_almacen; ?>"><?php echo strtoupper($almacen->nombre_almacen); ?></td>
                                <td id="direccion<?php echo $almacen->id_almacen; ?>"><?php echo strtoupper($almacen->direccion_almacen); ?></td>
                                <td id="telefono<?php echo $almacen->id_almacen; ?>"><?php echo strtoupper($almacen->telefono_almacen); ?></td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2 white-text" role="group" aria-label="First group">
                                            <a id="UpdateAlmacenModal" data-toggle="modal" onclick="UpdateAlmacen(<?php echo $almacen->id_almacen; ?>)" data-target="#UpdateModal" class="btn-sm amber darken-1"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                            <a id="DeleteAlmacenModal" onclick="DeleteAlmacen(<?php echo $almacen->id_almacen; ?>)" class="btn-sm red darken-1" data-toggle="modal" data-target="#modalConfirmDelete"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    <?php require_once RUTA_APP . '/views/datatables/dtfoot.php'; ?>
                    <?php require_once RUTA_APP . '/views/modals/modalUpdate.php'; ?>
                        <form id="UpdateAlmacen" class="text-center p-5" method="POST">
                            <p class="h4 mb-4">ALMACEN</p>
                            <div class="md-form form-sm">
                                <input type="text" id="formId" value=" " class="form-control" hidden>
                                <input type="text" id="formDesc" value=" " class="form-control" required>
                                <label for="form8" class="active">Nombre del almacen:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" id="formDireccion" value=" " class="form-control" require>
                                <label for="form8" class="active">Direccion del almacen</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" id="formTelefono" value=" " class="form-control" required>
                                <label for="form8" class="active">Telefono del almacen:</label>
                            </div>
                            <button class="btn btn-outline-warning btn-block" type="submit">Enviar</button>
                        </form>
                    <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                </div>  
            </div>  
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>