<?php require_once RUTA_APP . '/views/inc/header.php'; ?>
    <section>
    </section>
    <div class="table-responsive">
        <table id="dtBasicExample" class="table table-striped display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="th-sm">CORREO
                    </th>
                    <th class="th-sm">USUARIO
                    </th>
                    <th class="th-sm">CONTRASEÑA
                    </th>
                    <th class="th-sm">ROL
                    </th>
                    <th class="th-sm">ACCIONES
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php  foreach ($datos['usuarios'] as $usuario) : ?>
                <tr>
                    <td><?php echo $usuario->email ?></td>
                    <td><?php echo $usuario->user ?></td>
                    <td><?php echo $usuario->pass ?></td>
                    <td><?php echo strtoupper($usuario->desc_rol); ?></td>
                    <td>
                        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                <a href="<?php echo RUTA_URL;?>/pages/editar/<?php echo $usuario->email;?>" class="btn-sm amber darken-1"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                <a href="<?php echo RUTA_URL;?>/pages/borrar/<?php echo $usuario->email;?>" class="btn-sm red" data-toggle="modal" data-target="#modalConfirmDelete"><i class="fas fa-trash-alt" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>