<?php
//Incluímos el archivo Factura.php
require('Factura.php');

//Establecemos los datos de la empresa
$logo = "coolcenter";
$ext_logo = "png";
$empresa = "Cuboxti Informatica ";
$documento = "RFC:AEHM911003AN2";
$direccion = "2a Calle Ote. loc. 30 B \nentre 5ta. priv. y 5ta. sur Col. Centro";
$telefono = "962-62-53-292";
$email = "contacto@cuboxti.mx";
$web = "www.cuboxti.mx";


//Obtenemos los datos de la cabecera de la venta actual
require_once "../models/Inventario.php";
$hj= new Iventariaro();
$rsptav = $hj->ventacabecera($_GET["id"]);
//Recorremos todos los valores obtenidos
$regv = $rsptav->fetch_object();

//Establecemos la configuración de la factura
$pdf = new PDF_Invoice( 'P', 'mm', 'A4' );
$pdf->AddPage();

//Enviamos los datos de la empresa al método addSociete de la clase Factura
$pdf->addSociete(utf8_decode($empresa),
                  $documento."\n" .
                  utf8_decode("Dirección: ").utf8_decode($direccion)."\n".
                  utf8_decode("Teléfono: ").$telefono."\n" .
                  utf8_decode("Web: ").$web."\n" .
                  "Email : ".$email,$logo);
$pdf->fact_dev( "FOLIO ", "$regv->codigo" );
$pdf->temporaire( "" );
$pdf->addDate( $regv->fecha);

//Enviamos los datos del cliente al método addClientAdresse de la clase Factura
//$pdf->addClientAdresse(utf8_decode($regv->cliente),"Domicilio: ".utf8_decode($regv->direccion),"ID Cliente".": ".$regv->num_documento,"Email: ".$regv->email,"Telefono: ".$regv->telefono);
$pdf->addCliente("Nombre: ".utf8_decode($regv->cliente),"Domicilio: ".utf8_decode($regv->direccion),"Codigo de Cliente".": ".utf8_decode($regv->num_documento),"Correo: ".utf8_decode($regv->email),"Telefono: ".utf8_decode($regv->telefono));
$pdf->addDispositivos("Marca: ".utf8_decode($regv->marca),"Modelo".": ".$regv->modelo,"No. Serie: ".$regv->serie,"Tipo de Dispositivo".": ".$regv->dispositivotipo,"Dispositivo Entrega: ".$regv->dispositivoentrega);
$pdf->addReporte("Problemas que presenta el equipo: ".utf8_decode($regv->problema), "Observaciones: ".utf8_decode($regv->observacion));

//Establecemos las columnas que va a tener la sección donde mostramos los detalles de la venta
$cols=array( "SERVICIO"=>121,
             "P. SERVICIO"=>23,
             "DESCUENTO"=>23,
             "SUBTOTAL"=>23);
$pdf->addCols( $cols);
$cols=array( "SERVICIO"=>"L",
             "P. SERVICIO"=>"L",
             "DESCUENTO"=>"C",
             "SUBTOTAL"=>"C");
$pdf->addLineFormat( $cols);
//$pdf->addLineFormat($cols);
//Actualizamos el valor de la coordenada "y", que será la ubicación desde donde empezaremos a mostrar los datos
$y= 150;

//Obtenemos todos los detalles de la venta actual
$rsptad = $hj->ventadetalle($_GET["id"]);

while ($regd = $rsptad->fetch_object()) {
  $line = array( "SERVICIO"=>utf8_decode( "$regd->nombre"),
                "P. SERVICIO"=> utf8_decode("$regd->precio"),
                "DESCUENTO" => "$regd->descuento",
                "SUBTOTAL"=> "$regd->subtotal");
            $size = $pdf->addLine( $y, $line );
            $y   += $size + 2;
}
$pdf->addRecepcion(utf8_decode($regv->usuario));
$pdf->addentrega(utf8_decode($regv->cliente));
$pdf->addrecibi();
$pdf->addnota();

//Convertimos el total en letras
require_once "Letras.php";
$V=new EnLetras(); 
$con_letra=strtoupper($V->ValorEnLetras($regv->total,"PESOS MEXICANOS"));
$pdf->addCadreTVAs("".$con_letra);
$num=16;
//Mostramos el impuesto
$pdf->addTVAs( $regv->iva, $regv->total,"$ ");
$pdf->addCadreEurosFrancs("IVA"." $num %");
$pdf->Output('Hoja de Compras','I');
?>