<?php require_once RUTA_APP . '/views/inc/header.php'; ?>
            <div class="view view-cascade gradient-card-header blue lighten-1 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                <div></div>
                <a href="#" class="white-text mx-3">EMPLEADO</a>
                <div></div>
            </div>
            <div class="container">
                <div>
                    <a id="modalActivate" data-toggle="modal" data-target="#InsertModal" class="btn light-blue darken-3 waves-effect"><i class="far fa-plus-square" aria-hidden="true"></i> AGREGAR</a>
                </div>
                <?php require_once RUTA_APP . '/views/modals/modalInsert.php'; ?>
                    <form class="text-center p-5" id="InsertEmpleado" method="POST">
                        <p class="h4 mb-4">NUEVO EMPLEADO</p>
                        <div class="md-form form-sm">
                            <input type="text" name="desc" id="desc" class="form-control" required>
                            <label for="form8" class="active">Nombre del Empleado:</label>
                        </div>
                        <div class="md-form form-sm">
                            <select name="municipio" id="municipio" class="form-control" searchable="Busca Aqui.." required>
                                <?php  foreach ($datos['municipio'] as $municipio) : ?>
                                    <option value="<?php echo $municipio->id_municipio; ?>"><?php echo strtoupper($municipio->nombre_municipio); ?></option>
                                <?php endforeach;?>
                            </select>
                            <label for="form8" class="active">Nombre del municipio:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="cp" id="cp" class="form-control" required>
                            <label for="form8" class="active">Codigo postal:</label>
                        </div>
                        <div class="md-form form-sm">
                            <select name="estado" id="estado" class="form-control" searchable="Busca Aqui.." required>
                                <?php  foreach ($datos['estado'] as $estado) : ?>
                                    <option value="<?php echo $estado->id_estado; ?>"><?php echo strtoupper($estado->nombre_estado); ?></option>
                                <?php endforeach;?>
                            </select>
                            <label for="form8" class="active">Nombre del estado:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="telefono" id="telefono" class="form-control" required>
                            <label for="form8" class="active">Telefono:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="celular" id="celular" class="form-control" required>
                            <label for="form8" class="active">Celular:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="rfc" id="rfc" class="form-control" required>
                            <label for="form8" class="active">RFC:</label>
                        </div>
                       
                        <div class="md-form form-sm">
                            <input type="number" name="credito" id="credito" value="0" class="form-control" required>
                            <label for="form8" class="active">Credito:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="number" name="descuento" id="descuento" value="0" class="form-control" required>
                            <label for="form8" class="active">Descuento:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="direccion" id="direccion" class="form-control" required>
                            <label for="form8" class="active">Direccion:</label>
                        </div>
                        <div class="view view-cascade gradient-card-header blue lighten-1 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                        <div></div>
                            <a href="#" class="white-text mx-3">DATOS DE USUARIO</a>
                         <div></div>
                         </div>
                         <div class="md-form form-sm">
                            <input type="text" name="usuario" id="usuario" class="form-control" required>
                            <label for="form8" class="active">Usuario</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="email" name="email" id="email" class="form-control" required>
                            <label for="form8" class="active">Email:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="password" name="pass" id="pass"  class="form-control" required>
                            <label for="form8" class="active">Password:</label>
                        </div>
                        <div class="md-form form-sm">
                        <select name="nivel" id="nivel" class="form-control" searchable="Busca Aqui.." required>
                            <option value="1">Administrador</option>
                            <option value="2">Vendedor</option>
                        </select>
                            <label for="form8" class="active">Nivel de Usuario:</label>
                        </div>
                        <button class="btn btn-outline-primary btn-block" type="submit">Enviar</button>
                    </form>
                <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                <br />
                <div class="table-responsive">
                    <?php require_once RUTA_APP . '/views/datatables/dthead.php'; ?>
                        <thead>
                            <tr>
                                <th class="th-sm">NOMBRE</th>
                                <th class="th-sm">MUNICIPIO</th>
                                <th class="th-sm">CODIGO POSTAL</th>
                                <th class="th-sm">ESTADO</th>
                                <th class="th-sm">TELEFONO</th>
                                <th class="th-sm">RFC</th>
                                <th class="th-sm">EMAIL</th>
                                <th class="th-sm">CREDITO</th>
                                <th class="th-sm">DESCUENTO</th>
                                <th class="th-sm">DIRECCION</th>
                                <th class="th-sm">CELULAR</th>
                                <th class="th-sm">ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach ($datos['empleado'] as $empleado) : ?>
                            <tr>
                                <td id="desc<?php echo $empleado->id_persona; ?>"><?php echo strtoupper($empleado->nombre_persona); ?></td>
                                <td id="municipio<?php echo $empleado->id_persona; ?>"><?php echo strtoupper($empleado->nombre_municipio); ?></td>
                                <td id="cp<?php echo $empleado->id_persona; ?>"><?php echo $empleado->cp_persona; ?></td>
                                <td id="estado<?php echo $empleado->id_persona; ?>"><?php echo strtoupper($empleado->nombre_estado); ?></td>
                                <td id="telefono<?php echo $empleado->id_persona; ?>"><?php echo strtoupper($empleado->telefono_persona); ?></td>
                                <td id="rfc<?php echo $empleado->id_persona; ?>"><?php echo $empleado->rfc_persona; ?></td>
                                <td id="email<?php echo $empleado->id_persona; ?>"><?php echo strtoupper($empleado->email_persona); ?></td>
                                <td id="credito<?php echo $empleado->id_persona; ?>"><?php echo $empleado->credito_persona; ?></td>
                                <td id="descuento<?php echo $empleado->id_persona; ?>"><?php echo strtoupper($empleado->descuento); ?></td>
                                <td id="direccion<?php echo $empleado->id_persona; ?>"><?php echo strtoupper($empleado->direccion_persona); ?></td>
                                <td id="celular<?php echo $empleado->id_persona; ?>"><?php echo $empleado->celular_persona; ?></td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2 white-text" role="group" aria-label="First group">
                                            <a id="UpdateEmpleadoModal" data-toggle="modal" onclick="UpdateEmpleado(<?php echo $empleado->id_persona; ?>,<?php echo $empleado->id_municipio; ?>,<?php echo $empleado->id_estado ?>)" data-target="#UpdateModal" class="btn-sm amber darken-1"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                            <a id="DeleteEmpleadoModal" onclick="DeleteEmpleado(<?php echo $empleado->id_persona; ?>)" class="btn-sm red darken-1" data-toggle="modal" data-target="#modalConfirmDelete"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    <?php require_once RUTA_APP . '/views/datatables/dtfoot.php'; ?>
                    <?php require_once RUTA_APP . '/views/modals/modalUpdate.php'; ?>
                        <form id="UpdateEmpleado" class="text-center p-5" method="POST">
                            <p class="h4 mb-4">EMPLEADO</p>
                            <div class="md-form form-sm">
                                <input type="text" id="formId" value=" " class="form-control" hidden>
                                <input type="text" id="formDesc" value=" " class="form-control" required>
                                <label for="form8" class="active">Nombre del EMPLEADO:</label>
                            </div>
                            <div class="select-wrapper md-form form-sm">
                                <select class="browser-default custom-select" id="formMunicipio" searchable="Busca Aqui.." required>
                                    <option value="" disabled selected>Elegir Municipio</option>
                                    <?php  foreach ($datos['municipio'] as $municipio) : ?>
                                    <option value="<?php echo $municipio->id_municipio; ?>"><?php echo strtoupper($municipio->nombre_municipio); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre del municipio:</label>
                            </div>
                            <div class="md-form form-sm">
                                  <input type="text" id="formCP" value=" " class="form-control" required>
                                  <label for="form8" class="active">Codigo postal:</label>
                             </div>
                             <div class="select-wrapper md-form form-sm">
                                <select class="browser-default custom-select" id="formEstado" searchable="Busca Aqui.." required>
                                    <option value="" disabled selected>Elegir Estado</option>
                                    <?php  foreach ($datos['estado'] as $estado) : ?>
                                    <option value="<?php echo $estado->id_estado; ?>"><?php echo strtoupper($estado->nombre_estado); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre del estado:</label>
                            </div>
                            <div class="md-form form-sm">
                              <input type="text" id="formTelefono" value=" " class="form-control" >
                              <label for="form8" class="active">Telefono:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" id="formCelular" value=" " class="form-control" >
                                <label for="form8" class="active">Celular:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" id="formRFC" value=" " class="form-control">
                                <label for="form8" class="active">RFC:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="email" id="formEmail" value=" " class="form-control" >
                                <label for="form8" class="active">Email:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="number" id="formCredito" value="0" class="form-control" required>
                                <label for="form8" class="active">Credito:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="number" id="formDescuento" value="0" class="form-control" required>
                                <label for="form8" class="active">Descuento:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" id="formDireccion" value=" " class="form-control" required>
                                <label for="form8" class="active">Direccion:</label>
                            </div>
                            <button class="btn btn-outline-warning btn-block" type="submit">Enviar</button>
                        </form>
                    <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                </div>  
            </div>  
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>