<?php require_once RUTA_APP . '/views/inc/header.php'; ?>
            <div class="view view-cascade gradient-card-header blue lighten-1 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                <div></div>
                <a href="#" class="white-text mx-3">MUNICIPIO</a>
                <div></div>
            </div>
            <div class="container">
                <div>
                    <a id="modalActivate" data-toggle="modal" data-target="#InsertModal" class="btn light-blue darken-3 waves-effect"><i class="far fa-plus-square" aria-hidden="true"></i> AGREGAR</a>
                </div>
                <?php require_once RUTA_APP . '/views/modals/modalInsert.php'; ?>
                    <form class="text-center p-5" id="InsertMunicipio" method="POST">
                        <p class="h4 mb-4">NUEVA MUNICIPIO</p>
                        <div class="md-form form-sm">
                            <input type="text" name="desc" id="desc" class="form-control" required>
                            <label for="form8" class="active">Nombre del municipio:</label>
                        </div>
                        <div class="md-form form-sm">
                            <select name="estado" id="estado" class="form-control" searchable="Busca Aqui.." required>
                                <?php  foreach ($datos['estado'] as $estado) : ?>
                                    <option value="<?php echo $estado->id_estado; ?>"><?php echo strtoupper($estado->nombre_estado); ?></option>
                                <?php endforeach;?>
                            </select>
                            <label for="form8" class="active">Nombre del estado:</label><br><br>
                        </div>
                        <button class="btn btn-outline-primary btn-block" type="submit">Enviar</button>
                    </form>
                <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                <br />
                <div class="table-responsive">
                    <?php require_once RUTA_APP . '/views/datatables/dthead.php'; ?>
                        <thead>
                            <tr>
                                <th class="th-sm">NOMBRE DEL MUNICIPIO
                                </th>
                                <th class="th-sm">NOMBRE DEL ESTADO
                                </th>
                                <th hidden>
                                </th>
                                <th class="th-sm">ACCIONES
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach ($datos['municipio'] as $municipio) : ?>
                            <tr>
                                <td id="desc<?php echo $municipio->id_municipio; ?>"><?php echo strtoupper($municipio->nombre_municipio); ?></td>
                                <td id="estado<?php echo $municipio->id_municipio; ?>">
                                    <?php echo strtoupper($municipio->nombre_estado); ?>
                                </td>
                                <td id="id<?php echo $municipio->id_municipio; ?>" hidden>
                                    <?php echo $municipio->id_estado; ?>
                                </td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2 white-text" role="group" aria-label="First group">
                                            <a id="UpdateMunicipioModal" data-toggle="modal" onclick="UpdateMunicipio(<?php echo $municipio->id_municipio; ?>,<?php echo $municipio->id_estado ?>)" data-target="#UpdateModal" class="btn-sm amber darken-1"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                            <a id="DeleteMunicipioModal" onclick="DeleteMunicipio(<?php echo $municipio->id_municipio; ?>)" class="btn-sm red darken-1" data-toggle="modal" data-target="#modalConfirmDelete"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    <?php require_once RUTA_APP . '/views/datatables/dtfoot.php'; ?>
                    <?php require_once RUTA_APP . '/views/modals/modalUpdate.php'; ?>
                        <form id="UpdateMunicipio" class="text-center p-5" method="POST">
                            <p class="h4 mb-4">MUNICIPIO</p>
                            <div class="md-form form-sm">
                                <input type="text" id="formId" value=" " class="form-control" hidden>
                                <input type="text" id="formDesc" value=" " class="form-control" required>
                                <label for="form8" class="active">Nombre del Municipio:</label>
                            </div>
                            <div class="select-wrapper md-form form-sm">
                                <select class="browser-default custom-select" id="formEstado" searchable="Busca Aqui.." required>
                                    <option value="" disabled selected>Elegir Estado</option>
                                <?php  foreach ($datos['estado'] as $estado) : ?>
                                    <option value="<?php echo $estado->id_estado ?>"><?php echo strtoupper($estado->nombre_estado); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre de la estado:</label>
                            </div>
                            <button class="btn btn-outline-warning btn-block" type="submit">Enviar</button>
                        </form>
                    <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                </div>  
            </div>  
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>