<?php require_once RUTA_APP . '/views/inc/header.php'; ?>
            <div class="view view-cascade gradient-card-header blue lighten-1 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                <div></div>
                <a href="#" class="white-text mx-3">REPORTE DE COMPRAS</a>
                <div></div>
            </div>
            <div class="container-fluid">
                <br />
                <div class="row">
                    <div class="col-5">
                        <div class="md-form form-sm">
                            <input type="text" name="fechaInicio" value="<?php echo date('Y/m/d'); ?>" id="fechaInicioCompra" class="form-control datepicker fechadatepicker" required>
                            <label for="form8" class="active">Fecha Inicio de la Compra:</label>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="md-form form-sm">
                            <input type="text" name="fechaTope" placeholder="AÑO/MES/DIA" id="fechaTopeCompra" class="form-control datepicker fechadatepicker" required>
                            <label for="form8" class="active">Fecha Tope de la Compra:</label>
                        </div>
                    </div>
                    <div class="col-2">
                    <a id="BuscarReporteCompra" onclick="BuscarReporteCompra()" class="btn light-blue lighten waves-effect"><i class="fas fa-find" aria-hidden="true"></i> BUSCAR</a>
                    </div>
                </div>
                <br />
                <div class="table-responsive">
                    <?php require_once RUTA_APP . '/views/datatables/dthead.php'; ?>
                        <thead>
                            <tr>
                                <th class="th-sm">SERIE DE LA FACTURA
                                </th>
                                <th class="th-sm">VENDEDOR
                                </th>
                                <th class="th-sm">FECHA DE COMPRA
                                </th>
                                <th class="th-sm">PROVEEDOR DE COMPRA
                                </th>
                                <th class="th-sm">TOTAL DE COMPRA
                                </th>
                                <th class="th-sm">TIPO DE PAGO DE COMPRA
                                </th>
                                <th class="th-sm">ACCIONES
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach ($datos['compra'] as $compra) : ?>
                            <tr>
                                <td id="factura<?php echo $compra->id_compra; ?>"><?php echo strtoupper($compra->factura); ?></td>
                                <td id="vendedor<?php echo $compra->id_compra; ?>"><?php echo strtoupper($compra->vendedor); ?></td>
                                <td id="fecha<?php echo $compra->id_compra; ?>"><?php echo strtoupper($compra->fecha_compra); ?></td>
                                <td id="proveedor<?php echo $compra->id_compra; ?>"><?php echo strtoupper($compra->proveedor); ?></td>
                                <td id="total<?php echo $compra->id_compra; ?>"><?php echo strtoupper($compra->total_compra); ?></td>
                                <td id="tipopago<?php echo $compra->id_compra; ?>"><?php echo strtoupper($compra->nombre_tipo_pago); ?></td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2 white-text" role="group" aria-label="First group">
                                            <a id="UpdateCompraModal" onclick="UpdateCompra(<?php echo $compra->id_compra; ?>,<?php echo $compra->id_articulo; ?>,<?php echo $compra->id_almacen ?>)" data-target="#UpdateModal" class="btn-sm amber darken-1"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                            <a id="DeleteCompraModal" onclick="DeleteCompra(<?php echo $compra->id_compra; ?>)" class="btn-sm red darken-1" data-toggle="modal" data-target="#modalConfirmDelete"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                            <a id="ReporteCompraModal" onclick="ReporteCompra(<?php echo $compra->id_compra; ?>)" class="btn-sm blue darken" data-toggle="modal" data-target="#modalConfirmDelete"><i class="fas fa-file" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    <?php require_once RUTA_APP . '/views/datatables/dtfoot.php'; ?>
                </div>  
            </div>  
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>