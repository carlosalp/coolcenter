<?php require_once RUTA_APP . '/views/inc/header.php'; ?>
            <div class="view view-cascade gradient-card-header blue lighten-1 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                <div></div>
                <a href="#" class="white-text mx-3">INVENTARIO</a>
                <div></div>
            </div>
            <div class="container">
                <div>
                    <a id="modalActivate" data-toggle="modal" data-target="#InsertModal" class="btn light-blue darken-3 waves-effect"><i class="far fa-plus-square" aria-hidden="true"></i> AGREGAR</a>
                </div>
                <?php require_once RUTA_APP . '/views/modals/modalInsert.php'; ?>
                    <form class="text-center p-5" id="InsertInventario" method="POST">
                        <p class="h4 mb-4">NUEVO INVENTARIO</p>
                        <div class="md-form form-sm">
                        <select name="articulo" id="articulo" class="form-control" searchable="Busca Aqui.." required>
                                <?php  foreach ($datos['articulo'] as $articulo) : ?>
                                    <option value="<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->nombre_articulo); ?></option>
                                <?php endforeach;?>
                            </select>
                            <label for="form8" class="active">Nombre del Articulo :</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="number" name="cantidad" id="cantidad" class="form-control" required>
                            <label for="form8" class="active">Cantidad:</label>
                        </div>
                        <div class="md-form form-sm">
                            <select name="almacen" id="almacen" class="form-control" searchable="Busca Aqui.." required>
                                <?php  foreach ($datos['almacen'] as $almacen) : ?>
                                    <option value="<?php echo $almacen->id_almacen; ?>"><?php echo strtoupper($almacen->nombre_almacen); ?></option>
                                <?php endforeach;?>
                            </select>
                            <label for="form8" class="active">Nombre del Almacen :</label>
                        </div>
                        <button class="btn btn-outline-primary btn-block" type="submit">Enviar</button>
                    </form>
                <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                <br />
                <div class="table-responsive">
                    <?php require_once RUTA_APP . '/views/datatables/dthead.php'; ?>
                        <thead>
                            <tr>
                                <th class="th-sm">NOMBRE DEL ARTICULO
                                </th>
                                <th class="th-sm">DISPONIBILIDAD DE ARTICULOS
                                </th>
                                <th class="th-sm">NOMBRE DEL ALMACEN
                                </th>
                                <th class="th-sm">ACCIONES
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach ($datos['inventario'] as $inventario) : ?>
                            <tr>
                                <td id="articulo<?php echo $inventario->id_inventario; ?>"><?php echo strtoupper($inventario->nombre_articulo); ?></td>
                                <td id="cantidad<?php echo $inventario->id_inventario; ?>"><?php echo strtoupper($inventario->cantidad_articulo); ?></td>
                                <td id="almacen<?php echo $inventario->id_inventario; ?>"><?php echo $inventario->nombre_almacen; ?></td>
                                <td>
                                <?php  	$url='../../reportes/exInventario.php?id=';?>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2 white-text" role="group" aria-label="First group">
                                            <a id="UpdateInventarioModal" data-toggle="modal" onclick="UpdateInventario(<?php echo $inventario->id_inventario; ?>,<?php echo $inventario->id_articulo; ?>,<?php echo $inventario->id_almacen ?>)" data-target="#UpdateModal" class="btn-sm amber darken-1"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                            <a id="DeleteInventarioModal" onclick="DeleteInventario(<?php echo $inventario->id_inventario; ?>)" class="btn-sm red darken-1" data-toggle="modal" data-target="#modalConfirmDelete"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    <?php require_once RUTA_APP . '/views/datatables/dtfoot.php'; ?>
                    <?php require_once RUTA_APP . '/views/modals/modalUpdate.php'; ?>
                        <form id="UpdateInventario" class="text-center p-5" method="POST">
                            <p class="h4 mb-4">INVENTARIO</p>
                            <div class="md-form form-sm">
                                <input type="text" id="formId" value=" " class="form-control" hidden>
                                <select class="browser-default custom-select" id="formArticulo" searchable="Busca Aqui.." required>
                                    <option value="" disabled selected>Elegir un Articulo</option>
                                <?php  foreach ($datos['articulo'] as $articulo) : ?>
                                    <option value="<?php echo $articulo->id_articulo?>"><?php echo strtoupper($articulo->nombre_articulo); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre de la subcategoria:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="number" id="formCantidad" value="1" class="form-control" required>
                                <label for="for8" class="active">Cantidad de articulo:</label>
                            </div>
                            <div class="select-wrapper md-form form-sm">
                                <select class="browser-default custom-select" id="formAlmacen" searchable="Busca Aqui.." required>
                                    <option value="" disabled selected>Elegir Almacen</option>
                                <?php  foreach ($datos['almacen'] as $almacen) : ?>
                                    <option value="<?php echo $almacen->id_almacen ?>"><?php echo strtoupper($almacen->nombre_almacen); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre del Almacen:</label>
                            </div>
                        
                            <button class="btn btn-outline-warning btn-block" type="submit">Enviar</button>
                        </form>
                    <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                </div>  
            </div>  
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>