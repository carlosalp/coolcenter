<?php require_once RUTA_APP . '/views/inc/header.php'; ?>
            <div class="view view-cascade gradient-card-header blue lighten-1 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                <div></div>
                <a href="#" class="white-text mx-3">VENTAS</a>
                <div></div>
            </div>
            <?php require_once RUTA_APP . '/views/modals/modalInsert.php'; ?>
                <form class="text-center p-5" id="InsertArticulo" method="POST" enctype="multipart/form-data">
                    <p class="h4 mb-4">NUEVO ARTICULO</p>
                    <div class="md-form form-sm">
                        <input type="text" name="nombre" id="nombre" class="form-control" required>
                        <label for="form8" class="active">Nombre del articulo:</label>
                    </div>
                    <div class="md-form form-sm">
                        <input type="text" name="ns" id="ns" class="form-control" required>
                        <label for="form8" class="active">NS del articulo:</label>
                    </div>
                    <div class="md-form form-sm">
                        <input type="text" name="desc" id="desc" class="form-control" required>
                        <label for="form8" class="active">Descripcion del articulo:</label>
                    </div>
                    <div class="md-form form-sm">
                        <input type="text" name="modelo" id="modelo" class="form-control" required>
                        <label for="form8" class="active">Modelo del articulo:</label>
                    </div>
                    <div class="wrapper md-form form-sm">
                        <input type="number" name="precio_venta" id="precio_venta" class="form-control" required>
                        <label for="form8" class="active">Precio de venta del articulo:</label>
                    </div>
                    <div class="select-wrapper md-form form-sm">
                        <select class="browser-default custom-select" name="grupo" id="grupo" required>
                            <option value="" disabled selected>Elegir categoria</option>
                        <?php  foreach ($datos['grupo'] as $grupo) : ?>
                            <option value="<?php echo $grupo->id_grupo ?>"><?php echo strtoupper($grupo->nombre_grupo); ?></option>
                        <?php endforeach;?>
                        </select>
                        <label for="form8" class="active">Nombre de la categoria:</label>
                    </div>
                    <div class="select-wrapper md-form form-sm">
                        <select class="browser-default custom-select" name="subgrupo" id="subgrupo" required>
                            <option value="" disabled selected>Elegir subcategoria</option>
                        <?php  foreach ($datos['subgrupo'] as $subgrupo) : ?>
                            <option value="<?php echo $subgrupo->id_subgrupo ?>"><?php echo strtoupper($subgrupo->nombre_subgrupo); ?></option>
                        <?php endforeach;?>
                        </select>
                        <label for="form8" class="active">Nombre de la subcategoria:</label>
                    </div>
                    <div class="select-wrapper md-form form-sm">
                        <select class="browser-default custom-select" name="marca" id="marca" required>
                            <option value="" disabled selected>Elegir marca</option>
                        <?php  foreach ($datos['marca'] as $marca) : ?>
                            <option value="<?php echo $marca->id_marca ?>"><?php echo strtoupper($marca->nombre_marca); ?></option>
                        <?php endforeach;?>
                        </select>
                        <label for="form8" class="active">Nombre de la marca:</label>
                    </div>
                    <div class="wrapper md-form form-sm">
                        <input type="number" name="descuento" id="descuento" class="form-control" value="0" required>
                        <label for="form8" class="active">Descuento del articulo:</label>
                    </div>
                    <div class="select-wrapper md-form form-sm">
                        <select class="browser-default custom-select" name="unidad" id="unidad" required>
                            <option value="" disabled selected>Elegir tipo de unidad</option>
                        <?php  foreach ($datos['tipounidad'] as $tipounidad) : ?>
                            <option value="<?php echo $tipounidad->id_tipo_unidad ?>"><?php echo strtoupper($tipounidad->nombre_tipo_unidad); ?></option>
                        <?php endforeach;?>
                        </select>
                        <label for="form8" class="active">Nombre del tipo de unidad:</label>
                    </div>
                    <div class="custom-file wrapper md-form form-s">
                        <input type="file" name="imagen" class="custom-file-input" id="imagen">
                        <label class="custom-file-label" for="customFileLangHTML" data-browse="Subir">Imagen</label>
                    </div>
                    <button class="btn btn-outline-primary btn-block" type="submit">Enviar</button>
                </form>
            <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
            <div class="container-fluid">
                <!-- Stack the columns on mobile by making one full-width and the other half-width -->
                <form id="InsertVenta" method="POST">
                    <div class="row">
                        <div class="col">
                            <div class="md-form form-sm">
                                <input type="text" name="fecha" value="<?php echo date('Y/m/d'); ?>" id="fechaVenta" class="form-control datepicker fechadatepicker" required>
                                <label for="form8" class="active">Fecha de la Venta:</label>
                            </div>
                            <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
                            <div class="row">
                                <div class="col-6 col-md-4">
                                    <div class="md-form form-sm">
                                        <input type="text" name="factura" id="facturaVenta" class="form-control" required>
                                        <label for="form8" class="active">Factura de la Venta:</label>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Columns are always 50% wide, on mobile and desktop -->
                            <div class="row">
                                <div class="col-6">
                                    <div class="md-form form-sm">
                                        <select name="cliente" id="clienteVenta" class="form-control selectpicker select" data-live-search="true" data-style="btn-white" required>
                                                <option value="" disabled selected>Elegir cliente</option>
                                            <?php  foreach ($datos['cliente'] as $datoscliente) : ?>
                                                <option value="<?php echo $datoscliente->id_persona; ?>"><?php echo strtoupper($datoscliente->nombre_persona); ?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <label for="form8" class="active">Nombre del cliente:</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="md-form form-sm">
                                        <a id="AgregarCliente" data-toggle="modal" data-target="#InsertModalCliente" class="btn light-blue darken-3 waves-effect"><i class="fas fa-plus" aria-hidden="true"></i> CLIENTE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="md-form form-sm">
                                        <select name="vendedor" id="vendedorVenta" class="form-control selectpicker select" data-live-search="true" data-style="btn-white">
                                                <option value="" disabled selected>Elegir vendedor</option>
                                            <?php  foreach ($datos['vendedor'] as $datosvendedor) : ?>
                                                <option value="<?php echo $datosvendedor->id_persona; ?>"><?php echo strtoupper($datosvendedor->nombre_persona); ?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <label for="form8" class="active">Nombre del vendedor:</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="md-form form-sm">
                                        <select name="tipopago" id="tipopagoVenta" class="form-control selectpicker select" data-live-search="true" data-style="btn-white" required>
                                                <option value="" disabled selected>Elegir tipo de pago</option>
                                            <?php  foreach ($datos['tipopago'] as $datostipopago) : ?>
                                                <option value="<?php echo $datostipopago->id_tipo_pago; ?>"><?php echo strtoupper($datostipopago->nombre_tipo_pago); ?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <label for="form8" class="active">Tipo de Pago:</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                        <!-- Material form register -->
                            <div class="card card-form mt-2 mb-4">
                                <!--Card content-->
                                <div class="card-body rounded-top mdb-color">
                                    <h3 class="font-weight-bold text-center text-uppercase text-white mt-4">TOTAL</h3>
                                    <!-- First name -->
                                    <div class="md-form">
                                        <i class="fas fa-dollar-sign prefix text-white"></i>
                                        <input type="text" id="formTotal" name="formTotal" value=" " class="form-control text-white" required readonly>
                                        <label for="form8" class="text-white active">Total de la Venta:</label>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <br />
                    <hr />
                    <br />
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="md-form form-sm">
                                <select name="grupo" id="grupoVenta" class="form-control selectpicker select" onchange="articuloVentaCategoria()" data-live-search="true" data-style="btn-white">
                                        <option value="0" disabled selected>Elegir categoria</option>
                                    <?php  foreach ($datos['grupo'] as $grupo) : ?>
                                        <option value="<?php echo $grupo->id_grupo; ?>"><?php echo strtoupper($grupo->nombre_grupo); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre de la Categoria:</label>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="md-form form-sm">
                                <select name="articulo" id="articuloVenta" class="form-control selectpicker select" onchange="articuloChange()" data-live-search="true" data-style="btn-white">
                                        <option value="0" disabled selected>Elegir articulo</option>
                                    <!--<?php  foreach ($datos['articulo'] as $articulo) : ?>
                                        <option value="<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->nombre_articulo); ?></option>
                                    <?php endforeach;?>-->
                                </select>
                                <label for="form8" class="active">Nombre del Articulo:</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="md-form form-sm">
                                <a id="AgregarArticulo" data-toggle="modal" data-target="#InsertModal" class="btn light-blue darken-3 waves-effect"><i class="fas fa-plus" aria-hidden="true"></i> ARTICULO</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="md-form form-sm">
                                <input type="number" name="Precio" id="PrecioVenta" class="form-control">
                                <label for="form8" class="active">Precio de Venta del Articulo:</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form form-sm">
                                <input type="number" name="Cantidad" id="CantidadVenta" class="form-control">
                                <label for="form8" class="active">Cantidad de la Venta:</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="md-form form-sm">
                                <select name="almacenVenta" id="almacenVenta" class="form-control selectpicker select" onchange="inventarioVentaAlmacen()" data-live-search="true" data-style="btn-white" readonly>
                                        <option value="0" disabled selected>Elegir almacen</option>
                                    <?php  foreach ($datos['almacen'] as $almacen) : ?>
                                        <option value="<?php echo $almacen->id_almacen; ?>"><?php echo strtoupper($almacen->nombre_almacen); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre del Almacen:</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="md-form form-sm">
                                <input type="text" name="IdAlmacen" id="IdCantidadVentaAlmacen" class="form-control" readonly hidden>
                                <input type="number" name="CantidadAlmacen" id="CantidadVentaAlmacen" class="form-control" readonly>
                                <label for="form8" class="active">Cantidad Disponible en Almacen:</label>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                            <a id="AgregarArticulo" onclick="AgregarArticuloVenta()" class="btn light-blue darken-3 waves-effect"><i class="fas fa-plus" aria-hidden="true"></i> AGREGAR</a>
                            <a id="CancelarArticulo" onclick="CancelarArticuloVenta()" class="btn red darken-3 waves-effect"><i class="fas fa-trash" aria-hidden="true"></i> CANCELAR</a>
                        </div>
                    </div>
                    <br />
                    <hr />
                    <div class="md-form form-sm">
                        <button class="btn btn-success" type="submit"><i class="fas fa-shopping-cart" aria-hidden="true"></i> Realizar Venta</button>
                    </div>
                    <br />
                    <table id="VentaArticuloAdded" class="table table-striped table-bordered dt-responsive nowrap display" width="100%">
                        <thead>
                            <tr>
                                <th class="th-sm">ACCIONES
                                </th>
                                <th class="th-sm">NS DEL ARTICULO
                                </th>
                                <th class="th-sm">NOMBRE DEL ARTICULO
                                </th>
                                <th class="th-sm">CANTIDAD
                                </th>
                                <th class="th-sm">PRECIO DE COMPRA DEL ARTICULO
                                </th>
                                <th class="th-sm">PRECIO DE VENTA DEL ARTICULO
                                </th>
                                <th class="th-sm">CATEGORIA
                                </th>
                                <th class="th-sm">MARCA
                                </th>
                                <th class="th-sm">ALMACEN
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    <?php require_once RUTA_APP . '/views/datatables/dtfoot.php'; ?>
                </form>
            </div>
            <div class="modal fade" id="InsertModalCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
                <div class="modal-dialog cascading-modal" role="document">
                    <div class="modal-content">
                        <div class="modal-header light-blue darken-3 white-text">
                            <h4 class="title"><i class="far fa-plus-square"></i>AGREGAR</h4>
                            <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form class="text-center p-5" id="InsertCliente" method="POST">
                            <p class="h4 mb-4">NUEVO CLIENTE</p>
                            <div class="md-form form-sm">
                                <input type="text" name="desc" id="desc" class="form-control" required>
                                <label for="form8" class="active">Nombre del cliente:</label>
                            </div>
                            <div class="md-form form-sm">
                                <select name="municipio" id="municipio" class="form-control" searchable="Busca Aqui.." required>
                                    <?php  foreach ($datos['municipio'] as $municipio) : ?>
                                        <option value="<?php echo $municipio->id_municipio; ?>"><?php echo strtoupper($municipio->nombre_municipio); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre del municipio:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" name="cp" id="cp" class="form-control" required>
                                <label for="form8" class="active">Codigo postal:</label>
                            </div>
                            <div class="md-form form-sm">
                                <select name="estado" id="estado" class="form-control" searchable="Busca Aqui.." required>
                                    <?php  foreach ($datos['estado'] as $estado) : ?>
                                        <option value="<?php echo $estado->id_estado; ?>"><?php echo strtoupper($estado->nombre_estado); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre del estado:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" name="telefono" id="telefono" class="form-control" required>
                                <label for="form8" class="active">Telefono:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" name="celular" id="celular" class="form-control" required>
                                <label for="form8" class="active">Celular:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" name="rfc" id="rfc" class="form-control" required>
                                <label for="form8" class="active">RFC:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="email" name="email" id="email" class="form-control" required>
                                <label for="form8" class="active">Email:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="number" name="credito" id="credito" value="0" class="form-control" required>
                                <label for="form8" class="active">Credito:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="number" name="descuento" id="descuento" value="0" class="form-control" required>
                                <label for="form8" class="active">Descuento:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" name="direccion" id="direccion" class="form-control" required>
                                <label for="form8" class="active">Direccion:</label>
                            </div>
                    
                            <button class="btn btn-outline-primary btn-block" type="submit">Enviar</button>
                        </form>
                <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>