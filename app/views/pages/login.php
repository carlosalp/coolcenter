<?php require_once RUTA_APP . '/views/inc/headerLogin.php'; ?>
<!--Section: Live preview-->
<section class="form-dark">

<!--Form without header-->
<div class="card card-image" style="background-image: url('<?php echo RUTA_URL ?>/imagenes/img/WallpaperLogin.png'); width: 28rem;">
    <div class="text-white rgba-stylish-strong py-5 px-5 z-depth-4">

        <!--Header-->
        <div class="text-center">
            <h3 class="white-text mb-5 mt-4 font-weight-bold"><strong>INICIAR</strong> <a class="blue-text font-weight-bold"><strong> SESIÓN</strong></a></h3>
        </div>

        <!--Body-->
        <form id="Login" method="POST">
            <div class="md-form">
                <input type="text" id="formUsuario" name="usuario" class="white-text form-control" required>
                <label for="form8" class="active">Usuario:</label>
            </div>
            <div class="md-form">
                <input type="password" id="formContrasena" name="contrasena" class="white-text form-control" required>
                <label for="form8" class="active">Contraseña:</label>
            </div>
            <!--Grid row-->
            <div class="row d-flex align-items-center mb-4">

                <!--Grid column-->
                <div class="text-center mb-3 col-md-12">
                    <button class="btn btn-primary btn-block btn-rounded z-depth-1" type="submit">Iniciar Sersión</button>
                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->
        </form>

    </div>
</div>
<!--/Form without header-->

</section>
<!--Section: Live preview-->
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>