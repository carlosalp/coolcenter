<?php require_once RUTA_APP . '/views/inc/header.php'; ?>
            <div class="view view-cascade gradient-card-header blue lighten-1 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                <div></div>
                <a href="#" class="white-text mx-3">ARTICULOS</a>
                <div></div>
            </div>
            
                <div>
                    <a id="modalActivate" data-toggle="modal" data-target="#InsertModal" class="btn light-blue darken-3 waves-effect"><i class="far fa-plus-square" aria-hidden="true"></i> AGREGAR</a>
                </div>
            <div class="container-fluid">
                <?php require_once RUTA_APP . '/views/modals/modalInsert.php'; ?>
                    <form class="text-center p-5" id="InsertArticulo" method="POST" enctype="multipart/form-data">
                        <p class="h4 mb-4">NUEVO ARTICULO</p>
                        <div class="md-form form-sm">
                            <input type="text" name="nombre" id="nombre" class="form-control" required>
                            <label for="form8" class="active">Nombre del articulo:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="ns" id="ns" class="form-control" required>
                            <label for="form8" class="active">NS del articulo:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="desc" id="desc" class="form-control" required>
                            <label for="form8" class="active">Descripcion del articulo:</label>
                        </div>
                        <div class="md-form form-sm">
                            <input type="text" name="modelo" id="modelo" class="form-control" required>
                            <label for="form8" class="active">Modelo del articulo:</label>
                        </div>
                        <div class="wrapper md-form form-sm">
                            <input type="number" name="precio_venta" id="precio_venta" class="form-control" required>
                            <label for="form8" class="active">Precio de venta del articulo:</label>
                        </div>
                        <div class="select-wrapper md-form form-sm">
                            <select class="browser-default custom-select" name="grupo" id="grupo" required>
                                <option value="" disabled selected>Elegir categoria</option>
                            <?php  foreach ($datos['grupo'] as $grupo) : ?>
                                <option value="<?php echo $grupo->id_grupo ?>"><?php echo strtoupper($grupo->nombre_grupo); ?></option>
                            <?php endforeach;?>
                            </select>
                            <label for="form8" class="active">Nombre de la categoria:</label>
                        </div>
                        <div class="select-wrapper md-form form-sm">
                            <select class="browser-default custom-select" name="subgrupo" id="subgrupo" required>
                                <option value="" disabled selected>Elegir subcategoria</option>
                            <?php  foreach ($datos['subgrupo'] as $subgrupo) : ?>
                                <option value="<?php echo $subgrupo->id_subgrupo ?>"><?php echo strtoupper($subgrupo->nombre_subgrupo); ?></option>
                            <?php endforeach;?>
                            </select>
                            <label for="form8" class="active">Nombre de la subcategoria:</label>
                        </div>
                        <div class="select-wrapper md-form form-sm">
                            <select class="browser-default custom-select" name="marca" id="marca" required>
                                <option value="" disabled selected>Elegir marca</option>
                            <?php  foreach ($datos['marca'] as $marca) : ?>
                                <option value="<?php echo $marca->id_marca ?>"><?php echo strtoupper($marca->nombre_marca); ?></option>
                            <?php endforeach;?>
                            </select>
                            <label for="form8" class="active">Nombre de la marca:</label>
                        </div>
                        <div class="wrapper md-form form-sm">
                            <input type="number" name="descuento" id="descuento" class="form-control" value="0" required>
                            <label for="form8" class="active">Descuento del articulo:</label>
                        </div>
                        <div class="select-wrapper md-form form-sm">
                            <select class="browser-default custom-select" name="unidad" id="unidad" required>
                                <option value="" disabled selected>Elegir tipo de unidad</option>
                            <?php  foreach ($datos['tipounidad'] as $tipounidad) : ?>
                                <option value="<?php echo $tipounidad->id_tipo_unidad ?>"><?php echo strtoupper($tipounidad->nombre_tipo_unidad); ?></option>
                            <?php endforeach;?>
                            </select>
                            <label for="form8" class="active">Nombre del tipo de unidad:</label>
                        </div>
                        <div class="custom-file wrapper md-form form-s">
                            <input type="file" name="imagen" class="custom-file-input" id="imagen">
                            <label class="custom-file-label" for="customFileLangHTML" data-browse="Subir">Imagen</label>
                        </div>
                        <button class="btn btn-outline-primary btn-block" type="submit">Enviar</button>
                    </form>
                <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                <br />
                <div class="table-responsive">
                    <?php require_once RUTA_APP . '/views/datatables/dthead.php'; ?>
                        <thead>
                            <tr>
                                <th class="th-sm">NS DEL ARTICULO
                                </th>
                                <th class="th-sm">NOMBRE DEL ARTICULO
                                </th>
                                <th class="th-sm">DESCRIPCION DEL ARTICULO
                                </th>
                                <th class="th-sm">MODELO DEL ARTICULO
                                </th>
                                <th class="th-sm">PRECIO DE VENTA DEL ARTICULO
                                </th>
                                <th class="th-sm">IMAGEN DEL ARTICULO
                                </th>
                                <th class="th-sm">GRUPO DEL ARTICULO
                                </th>
                                <th class="th-sm">SUBGRUPO DEL ARTICULO
                                </th>
                                <th class="th-sm">MARCA DEL ARTICULO
                                </th>
                                <th class="th-sm">DESCUENTO DEL ARTICULO
                                </th>
                                <th class="th-sm">TIPO DE UNIDAD DEL ARTICULO
                                </th>
                                <th class="th-sm">ACCIONES
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach ($datos['articulo'] as $articulo) : ?>
                            <tr>
                                <td id="ns<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->ns_articulo); ?></td>
                                <td id="nombre<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->nombre_articulo); ?></td>
                                <td id="descripcion<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->articulo_descripcion); ?></td>
                                <td id="modelo<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->articulo_modelo); ?></td>
                                <td id="precioventa<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->precio_venta); ?></td>
                                <td id="imagen<?php echo $articulo->id_articulo; ?>"><img src="<?php echo RUTA_URL ?>/imagenes/articulos/<?php echo $articulo->ruta_imagen; ?>" width="40px" height="40px"></td>
                                <td id="grupo<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->nombre_grupo); ?></td>
                                <td id="subgrupo<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->nombre_subgrupo); ?></td>
                                <td id="marca<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->nombre_marca); ?></td>
                                <td id="descuento<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->descuento); ?></td>
                                <td id="unidad<?php echo $articulo->id_articulo; ?>"><?php echo strtoupper($articulo->nombre_tipo_unidad); ?></td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2 white-text" role="group" aria-label="First group">
                                            <a id="UpdateArticuloModal" data-toggle="modal" onclick="UpdateArticulo(<?php echo $articulo->id_articulo; ?>, <?php echo $articulo->id_grupo; ?>, <?php echo $articulo->id_subgrupo; ?>, <?php echo $articulo->id_marca; ?>, <?php echo $articulo->unidad; ?>, '<?php echo $articulo->ruta_imagen; ?>')" data-target="#UpdateModal" class="btn-sm amber darken-1"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                            <a id="DeleteArticuloModal" onclick="DeleteArticulo(<?php echo $articulo->id_articulo; ?>)" class="btn-sm red darken-1" data-toggle="modal" data-target="#modalConfirmDelete"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    <?php require_once RUTA_APP . '/views/datatables/dtfoot.php'; ?>
                    <?php require_once RUTA_APP . '/views/modals/modalUpdate.php'; ?>
                        <form id="UpdateArticulo" class="text-center p-5" method="POST">
                            <p class="h4 mb-4">ARTICULO</p>
                            <div class="md-form form-sm">
                                <input type="text" id="formId" name="id" value=" " class="form-control" hidden>
                                <input type="text" id="formNom" name="nombre" value=" " class="form-control" required>
                                <label for="form8" class="active">Nombre del Articulo:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" id="formNS" name="ns" value=" " class="form-control" required>
                                <label for="form8" class="active">Número de Serie del articulo:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" id="formDesc" name="desc" value=" " maxlength="13" minlength="7" class="form-control" required>
                                <label for="form8" class="active">Descripcion del articulo:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="text" id="formModel" name="modelo" value=" " class="form-control" required>
                                <label for="form8" class="active">Modelo del articulo:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="number" id="formPrecio" name="precio_venta" value=" " class="form-control" required>
                                <label for="form8" class="active">Precio de venta del articulo:</label>
                            </div>
                            <div class="select-wrapper md-form form-sm">
                                <select class="browser-default custom-select" id="formGrupo" name="grupo" required>
                                    <option value="" disabled selected>Elegir categoria</option>
                                <?php  foreach ($datos['grupo'] as $grupo) : ?>
                                    <option value="<?php echo $grupo->id_grupo ?>"><?php echo strtoupper($grupo->nombre_grupo); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre de la categoria:</label>
                            </div>
                            <div class="select-wrapper md-form form-sm">
                                <select class="browser-default custom-select" id="formSubgrupo" name="subgrupo" required>
                                    <option value="" disabled selected>Elegir subcategoria</option>
                                <?php  foreach ($datos['subgrupo'] as $subgrupo) : ?>
                                    <option value="<?php echo $subgrupo->id_subgrupo ?>"><?php echo strtoupper($subgrupo->nombre_subgrupo); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre de la subcategoria:</label>
                            </div>
                            <div class="select-wrapper md-form form-sm">
                                <select class="browser-default custom-select" id="formMarca" name="marca" required>
                                    <option value="" disabled selected>Elegir marca</option>
                                <?php  foreach ($datos['marca'] as $marca) : ?>
                                    <option value="<?php echo $marca->id_marca ?>"><?php echo strtoupper($marca->nombre_marca); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre de la marca:</label>
                            </div>
                            <div class="md-form form-sm">
                                <input type="number" id="formDescuento" name="descuento" value="0" class="form-control" required>
                                <label for="form8" class="active">Descuento del articulo:</label>
                            </div>
                            <div class="select-wrapper md-form form-sm">
                                <select class="browser-default custom-select" id="formTipoUnidad" name="unidad" required>
                                    <option value="" disabled selected>Elegir tipo de unidad</option>
                                <?php  foreach ($datos['tipounidad'] as $tipounidad) : ?>
                                    <option value="<?php echo $tipounidad->id_tipo_unidad ?>"><?php echo strtoupper($tipounidad->nombre_tipo_unidad); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre de la marca:</label>
                            </div>
                            <input type="text" id="formImagen" name="img" hidden>
                            <div class="custom-file wrapper md-form form-s">
                                <input type="file" name="imagen" class="custom-file-input" accept="image/*">
                                <label class="custom-file-label" for="customFileLangHTML" data-browse="Subir">Imagen</label>
                            </div>
                            <button class="btn btn-outline-warning btn-block" type="submit">Enviar</button>
                        </form>
                    <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                </div>  
            </div>  
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>