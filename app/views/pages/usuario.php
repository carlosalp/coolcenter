<?php require_once RUTA_APP . '/views/inc/header.php'; ?>
            <div class="view view-cascade gradient-card-header blue lighten-1 narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">
                <div></div>
                <a href="#" class="white-text mx-3">USUARIOS</a>
                <div></div>
            </div>
            <div class="container">
                <div>
                    <a id="modalActivate" data-toggle="modal" data-target="#InsertModal" class="btn light-blue darken-3 waves-effect"><i class="far fa-plus-square" aria-hidden="true"></i> AGREGAR</a>
                </div>
                <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                <br />
                <div class="table-responsive">
                    <?php require_once RUTA_APP . '/views/datatables/dthead.php'; ?>
                        <thead>
                            <tr>
                                <th class="th-sm">NOMBRE
                                </th>
                                <th class="th-sm">NOMBRE DEL USUARIO
                                </th>
                                <th class="th-sm">NOMBRE DEL 
                                </th>
                                <th class="th-sm">ACCIONES
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach ($datos['usuario'] as $usuario) : ?>
                            <tr>
                                <td id="nombre<?php echo $usuario->id; ?>"><?php echo strtoupper($usuario->nombre_persona); ?></td>
                                <td id="user<?php echo $usuario->id; ?>"><?php echo strtoupper($usuario->username); ?>
                                <td id="email<?php echo $usuario->id; ?>"><?php echo strtoupper($usuario->email); ?></td>
                                <td id="pass<?php echo $usuario->id; ?>"><?php echo strtoupper($usuario->username); ?>
                                </td>
                                <td>
                                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                        <div class="btn-group mr-2 white-text" role="group" aria-label="First group">
                                            <a id="UpdateSubgrupoModal" data-toggle="modal" onclick="UpdateSubgrupo(<?php echo $subgrupo->id_subgrupo; ?>,<?php echo $subgrupo->id_grupo ?>)" data-target="#UpdateModal" class="btn-sm amber darken-1"><i class="fas fa-edit" aria-hidden="true"></i></a>
                                            <a id="DeleteSubgrupoModal" onclick="DeleteSubgrupo(<?php echo $subgrupo->id_subgrupo; ?>)" class="btn-sm red darken-1" data-toggle="modal" data-target="#modalConfirmDelete"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    <?php require_once RUTA_APP . '/views/datatables/dtfoot.php'; ?>
                    <?php require_once RUTA_APP . '/views/modals/modalUpdate.php'; ?>
                        <form id="UpdateSubgrupo" class="text-center p-5" method="POST">
                            <p class="h4 mb-4">SUBCATEGORIA</p>
                            <div class="md-form form-sm">
                                <input type="text" id="formId" value=" " class="form-control" hidden>
                                <input type="text" id="formDesc" value=" " class="form-control" required>
                                <label for="form8" class="active">Nombre de la subcategoria:</label>
                            </div>
                            <div class="select-wrapper md-form form-sm">
                                <select class="browser-default custom-select" id="formGrupo" searchable="Busca Aqui.." required>
                                    <option value="" disabled selected>Elegir categoria</option>
                                <?php  foreach ($datos['grupo'] as $datosgrupo) : ?>
                                    <option value="<?php echo $datosgrupo->id_grupo ?>"><?php echo strtoupper($datosgrupo->nombre_grupo); ?></option>
                                <?php endforeach;?>
                                </select>
                                <label for="form8" class="active">Nombre de la categoria:</label>
                            </div>
                            <button class="btn btn-outline-warning btn-block" type="submit">Enviar</button>
                        </form>
                    <?php require_once RUTA_APP . '/views/modals/modalF.php'; ?>
                </div>  
            </div>  
<?php require_once RUTA_APP . '/views/inc/footer.php'; ?>