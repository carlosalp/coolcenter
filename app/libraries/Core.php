<?php  
    /*
    Mapear la URL ingresada en el Navegador,
    1.- Controllador
    2.- Metodo
    3.- Parametro
    Ejemplo /articulos/actualizar/4
    */

    class Core{
        protected $controllerActual = 'pages';
        protected $methodActual = 'index';
        protected $parameters = [];
        
        public function __construct(){
            //print_r($this->getUrl());
            $url = $this->getUrl();
            //Buscar e controllers si el controlador existe
            if (file_exists('../app/controllers/' .ucwords($url[0]).'.php')) {
                //Si existe se setea como controlador por defecto
                $this->controllerActual = ucwords($url[0]);

                //unset indice
                unset($url[0]);
            }
            //requerrir el controlados
            require_once '../app/controllers/'. $this->controllerActual . '.php';
            $this->controllerActual = new $this->controllerActual;

            if (isset($url[1])) {
                //Verificar la segunda parte de la url que seria el metodo del controlador
                if (method_exists($this->controllerActual, $url[1])) {
                    //Verificamos el método
                    $this->methodActual = $url[1];
                    unset($url[1]);
                }
            }

            //echo $this->methodActual;

            //Obtener los parametros
            $this->parameters = $url ? array_values($url) : [];

            //llamar callback con parametros array
            call_user_func_array([$this->controllerActual, $this->methodActual], $this->parameters);
        }

        public function getUrl(){
            //echo $_GET['url'];
            if (isset($_GET['url'])) {
                session_start();
                if (isset($_SESSION['usuario']) || isset($_COOKIE['user'])){
                    if($_GET['url'] == "pages/login"){
                        $url = rtrim('pages/start', '/');
                    }
                    else{
                        if((($_COOKIE['nivel'] == 2) && ($_GET['url'] == "pages/venta"))){
                            $url = rtrim($_GET['url'], '/');
                        }
                        else if((($_COOKIE['nivel'] == 2) && ($_GET['url'] == "pages/compra"))){
                            $url = rtrim($_GET['url'], '/');
                        }
                        else if((($_COOKIE['nivel'] == 2) && ($_GET['url'] == "pages/cerrar"))){
                            $url = rtrim($_GET['url'], '/');
                        }
                        else if(($_COOKIE['nivel'] == 1)){
                            $url = rtrim($_GET['url'], '/');
                        }
                        else{
                            $url = rtrim('pages/start', '/');
                        }
                    }
                }
                else{
                    $url = rtrim('pages/login', '/');
                }
                
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode('/', $url);
                
                return $url;
            }
        }
    }
?>